#ifndef DEPGRAMMAR_H
#define DEPGRAMMAR_H

// for pccts
#include "pcctscfg.h"
#include "pccts_stdio.h"
#include "dtokens.h"
#include "AParser.h"
#include "DLexerBase.h"
#include "ATokPtr.h"
#include "deptree/DepTreeParser.h"
#include "deptree/DDlgLexer.h" 

#endif /* DEPGRAMMAR_H */

