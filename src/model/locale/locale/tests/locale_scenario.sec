/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "common/common_scenario.seh"
#include "locale/locale/tests/locale_scenario.seh"


/********************************************************************/
/**                       Test Scenario Data                       **/
/********************************************************************/
static CallContext context;


/********************************************************************/
/**                         Auxiliary Types                        **/
/********************************************************************/


/********************************************************************/
/**                     Model State Operations                     **/
/********************************************************************/


/********************************************************************/
/**                  Test Scenario Initialization                  **/
/********************************************************************/

bool init_locale_scenario(int argc, char** argv)
{
    context = getContext();

    return true;
}

void finish_locale_scenario(void)
{
}

/********************************************************************/
/**                          Test Actions                          **/
/********************************************************************/

scenario
bool locale_scen()
{
    CString* oldLocale;

    oldLocale = setlocale_spec(context, SUT_LC_ALL, NULL);

    setlocale_spec(context, SUT_LC_ALL, _CS(""));
    setlocale_spec(context, SUT_LC_COLLATE, _CS("POSIX"));
    setlocale_spec(context, SUT_LC_MESSAGES, _CS("ru.RU"));
    setlocale_spec(context, SUT_LC_MONETARY, _CS("en.EN"));
    setlocale_spec(context, SUT_LC_NUMERIC, _CS("qWeRtY"));
    setlocale_spec(context, SUT_LC_TIME, _CS(""));

    setlocale_spec(context, SUT_LC_ALL, oldLocale);

    return true;
}

scenario
bool thread_locales_scen()
{
    IntT i;
    LocaleT newlocs[5], duplocs[5], oldlocale;

    char strArray[6][5] =
    {
        "",
        "POSIX",
        "ru.RU",
        "en.EN",
        ""
    };

    IntT catMasks[5] =
    {
        SUT_LC_ALL,
        SUT_LC_MONETARY,
        SUT_LC_COLLATE,
        SUT_LC_CTYPE,
        SUT_LC_NUMERIC
    };

    oldlocale = uselocale_spec(context, NULL_VoidTPtr);

    for(i = 0; i < 5; i++)
    {
        newlocs[i] = newlocale_spec(context, catMasks[i], _CS(strArray[i]), NULL_VoidTPtr);

        if(!isNULL_VoidTPtr(newlocs[i]))
        {
            uselocale_spec(context, newlocs[i]);
            duplocs[i] = duplocale_spec(context, newlocs[i]);

            if(!isNULL_VoidTPtr(duplocs[i])) uselocale_spec(context, duplocs[i]);
        }
        else duplocs[i] = NULL_VoidTPtr;
    }

    uselocale_spec(context, oldlocale);

    for(i = 0; i < 5; i++)
    {
	    if(!isNULL_VoidTPtr(newlocs[i])) freelocale_spec(context, newlocs[i]);
        if(!isNULL_VoidTPtr(duplocs[i])) freelocale_spec(context, duplocs[i]);
    }

    return true;
}

scenario
bool localeconv_scen(void)
{
    LConv* lc;
    LocaleT locale;

    locale = newlocale_spec(context, SUT_LC_ALL, _CS("ru_RU"), NULL_VoidTPtr);
    uselocale_spec(context, locale);

    lc = localeconv_spec(context);

    return true;
}

scenario
bool nl_langinfo_scen(void)
{
    int i;

    for(i = SUT_DAY_1; i < SUT_DAY_7; i++) nl_langinfo_spec(context, i);

    for(i = SUT_ABDAY_1; i < SUT_ABDAY_7; i++) nl_langinfo_spec(context, i);

    for(i = SUT_MON_1; i < SUT_MON_12; i++) nl_langinfo_spec(context, i);

    for(i = SUT_ABMON_1; i < SUT_ABMON_12; i++) nl_langinfo_spec(context, i);

    nl_langinfo_spec(context, SUT_AM_STR);
    nl_langinfo_spec(context, SUT_PM_STR);

    nl_langinfo_spec(context, SUT_D_T_FMT);
    nl_langinfo_spec(context, SUT_D_FMT);
    nl_langinfo_spec(context, SUT_T_FMT);
    nl_langinfo_spec(context, SUT_T_FMT_AMPM);

    nl_langinfo_spec(context, SUT_ERA);
    nl_langinfo_spec(context, SUT_ERA_D_FMT);
    nl_langinfo_spec(context, SUT_ALT_DIGITS);
    nl_langinfo_spec(context, SUT_ERA_D_T_FMT);
    nl_langinfo_spec(context, SUT_ERA_T_FMT);

    nl_langinfo_spec(context, SUT_ALT_DIGITS);

    nl_langinfo_spec(context, SUT_RADIXCHAR);
    nl_langinfo_spec(context, SUT_THOUSEP);

    nl_langinfo_spec(context, SUT_YESEXPR);
    nl_langinfo_spec(context, SUT_NOEXPR);

    nl_langinfo_spec(context, SUT_CRNCYSTR);

    return true;
}

/********************************************************************/
/**                    Test Scenario Definition                    **/
/********************************************************************/

scenario ndfsm locale_locale_scenario =
{
    .init = init_locale_scenario,
    .finish = finish_locale_scenario,

    .actions = {
        locale_scen,
        thread_locales_scen,
        localeconv_scen,
        nl_langinfo_scen,
        NULL
    }
};
