/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Portions of this text are reprinted and reproduced in electronic form
 * from IEEE Std 1003.1, 2004 Edition, Standard for Information Technology
 * -- Portable Operating System Interface (POSIX), The Open Group Base
 * Specifications Issue 6, Copyright (C) 2001-2004 by the Institute of
 * Electrical and Electronics Engineers, Inc and The Open Group. In the
 * event of any discrepancy between this version and the original IEEE and
 * The Open Group Standard, the original IEEE and The Open Group Standard
 * is the referee document. The original Standard can be obtained online at
 * http://www.opengroup.org/unix/online.html.
 */



#include "locale/ctype/ctype_model.seh"
#include "locale/locale/locale_model.seh"

#pragma SEC subsystem ctype "locale.ctype"


/*
   The group of functions 'locale.ctype' consists of:
       __ctype_b_loc(GLIBC_2.3) [1]
       __ctype_get_mb_cur_max [1]
       isalnum [2]
       isalpha [2]
       isascii [2]
       isblank [2]
       iscntrl [2]
       isdigit [2]
       isgraph [2]
       islower [2]
       isprint [2]
       ispunct [2]
       isspace [2]
       isupper [2]
       isxdigit [2]
 */

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

NAME
    __ctype_b_loc -- accessor function for __ctype_b array for ctype functions

SYNOPSIS

    #include <ctype.h>

    const unsigned short * * __ctype_b_loc (void);

DESCRIPTION

    The __ctype_b_loc() function shall return a pointer into an array of
    characters in the current locale that contains characteristics for each
    character in the current character set. The array shall contain a total of
    384 characters, and can be indexed with any signed or unsigned char (i.e.
    with an index value between -128 and 255). If the application is
    multithreaded, the array shall be local to the current thread.

    This interface is not in the source standard; it is only in the binary
    standard.

RETURN VALUE

    The __ctype_b_loc() function shall return a pointer to the array of
    characters to be used for the ctype() family of functions (see <ctype.h>).

*/

specification
LocSpec* __ctype_b_loc_spec(CallContext context)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ReturnPointerToArrayForCtypeFamily,
                 "Return a pointer to the array of characters to be used for the ctype() family of functions"
               };
    }
    post
    {
        /*
         * The __ctype_b_loc() function shall return a pointer into an array of
         * characters in the current locale that contains characteristics for each
         * character in the current character set.
         */
        IMPLEMENT_REQ("__ctype_b_loc.01");

        /*
         * The array shall contain a total of 384 characters, and can be indexed with
         * any signed or unsigned char (i.e. with an index value between -128 and 255).
         */
        IMPLEMENT_REQ("__ctype_b_loc.02");

        /*
         * If the application is multithreaded, the array shall be local to the current
         * thread.
         */
        REQ("__ctype_b_loc.03", "", TODO_REQ());

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

NAME
    __ctype_get_mb_cur_max -- maximum length of a multibyte character in the
    current locale

SYNOPSIS

    size_t __ctype_get_mb_cur_max(void);

DESCRIPTION

    __ctype_get_mb_cur_max() returns the maximum length of a multibyte character
    in the current locale.

    __ctype_get_mb_cur_max() is not in the source standard; it is only in the
    binary standard.

*/

specification
SizeT __ctype_get_mb_cur_max_spec(CallContext context)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ReturnsMaximumLengthOfMultibyteCharacter, "Returns the maximum length of a multibyte character" };
    }
    post
    {
        /*
         * __ctype_get_mb_cur_max() returns the maximum length of a multibyte character
         * in the current locale.
         */
        REQ("__ctype_get_mb_cur_max.01", "", TODO_REQ());

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isalnum - test for an alphanumeric character

SYNOPSIS

    #include <ctype.h>

    int isalnum(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isalnum() function shall test whether c is a character of class alpha or
    digit in the program's current locale; see the Base Definitions volume of
    IEEE Std 1003.1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    representable as an unsigned char or equal to the value of the macro EOF. If
    the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isalnum() function shall return non-zero if c is an alphanumeric
    character; otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isalnum_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is
         * representable as an unsigned char or equal to the value of the macro EOF. If
         * the argument has any other value, the behavior is undefined.
         */
        REQ("app.isalnum.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("alnum"), c))
        {
        /*
         * The isalnum() function shall test whether c is a character of class alpha or
         * digit in the program's current locale
         *
         * The isalnum() function shall return non-zero if c is an alphanumeric character
         */
        REQ("isalnum.01.01", "shall return non-zero", isalnum_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("alnum"), c))
        {
        /*
         * The isalnum() function shall test whether c is a character of class alpha or
         * digit in the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isalnum.01.02", "shall return 0", isalnum_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isalpha - test for an alphabetic character

SYNOPSIS

    #include <ctype.h>

    int isalpha(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isalpha() function shall test whether c is a character of class alpha in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    representable as an unsigned char or equal to the value of the macro EOF. If
    the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isalpha() function shall return non-zero if c is an alphabetic
    character; otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isalpha_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is
         * representable as an unsigned char or equal to the value of the macro EOF. If
         * the argument has any other value, the behavior is undefined.
         */
        REQ("app.isalpha.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("alpha"), c))
        {
        /*
         * The isalpha() function shall test whether c is a character of class alpha in
         * the program's current locale
         *
         * The isalpha() function shall return non-zero if c is an alphabetic character
         */
        REQ("isalpha.01.01", "shall return non-zero", isalpha_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("alpha"), c))
        {
        /*
         * The isalpha() function shall test whether c is a character of class alpha in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isalpha.01.02", "shall return 0", isalpha_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isascii - test for a 7-bit US-ASCII character

SYNOPSIS

    #include <ctype.h>

    int isascii(int c);

DESCRIPTION

    The isascii() function shall test whether c is a 7-bit US-ASCII character
    code.

    The isascii() function is defined on all integer values.

RETURN VALUE

    The isascii() function shall return non-zero if c is a 7-bit US-ASCII
    character code between 0 and octal 0177 inclusive; otherwise, it shall
    return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isascii_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The isascii() function is defined on all integer values.
         */
        REQ("app.isascii.02", "c should be an integer value", true);

        return true;
    }
    post
    {
        if ((c >= 0) && (c <= 0177))
        {
            /*
             * The isascii() function shall test whether c is a 7-bit US-ASCII character code.
             *
             * The isascii() function shall return non-zero if c is a 7-bit US-ASCII character
             * code between 0 and octal 0177 inclusive
             */
            REQ("isascii.01.01", "isascii() should return non-zero", isascii_spec != 0);
        }
        else
        {
            /*
             * The isascii() function shall test whether c is a 7-bit US-ASCII character code.
             *
             * otherwise, it shall return 0.
             */
            REQ("isascii.01.02", "isascii() should return zero", isascii_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isblank - test for a blank character

SYNOPSIS

    #include <ctype.h>

    int isblank(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isblank() function shall test whether c is a character of class blank in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is a type int, the value of which the application shall
    ensure is a character representable as an unsigned char or equal to the
    value of the macro EOF. If the argument has any other value, the behavior
    is undefined.

RETURN VALUE

    The isblank() function shall return non-zero if c is a <blank>; otherwise,
    it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isblank_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is a type int, the value of which the application shall ensure
         * is a character representable as an unsigned char or equal to the value of the
         * macro EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isblank.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("blank"), c))
        {
        /*
         * The isblank() function shall test whether c is a character of class blank in
         * the program's current locale
         *
         * The isblank() function shall return non-zero if c is a <blank>
         */
        REQ("isblank.01.01", "shall return non-zero", isblank_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("blank"), c))
        {
        /*
         * The isblank() function shall test whether c is a character of class blank in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isblank.01.02", "shall return 0", isblank_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    iscntrl - test for a control character

SYNOPSIS

    #include <ctype.h>

    int iscntrl(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The iscntrl() function shall test whether c is a character of class cntrl in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is a type int, the value of which the application shall
    ensure is a character representable as an unsigned char or equal to the
    value of the macro EOF. If the argument has any other value, the behavior
    is undefined.

RETURN VALUE

    The iscntrl() function shall return non-zero if c is a control character;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT iscntrl_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is a type int, the value of which the application shall ensure
         * is a character representable as an unsigned char or equal to the value of the
         * macro EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.iscntrl.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("cntrl"), c))
        {
        /*
         * The iscntrl() function shall test whether c is a character of class cntrl in
         * the program's current locale
         *
         * The iscntrl() function shall return non-zero if c is a control character
         */
        REQ("iscntrl.01.01", "shall return non-zero", iscntrl_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("cntrl"), c))
        {
        /*
         * The iscntrl() function shall test whether c is a character of class cntrl in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("iscntrl.01.02", "shall return 0", iscntrl_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isdigit - test for a decimal digit

SYNOPSIS

    #include <ctype.h>

    int isdigit(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isdigit() function shall test whether c is a character of class digit in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isdigit() function shall return non-zero if c is a decimal digit;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isdigit_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isdigit.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("digit"), c))
        {
        /*
         * The isdigit() function shall test whether c is a character of class digit in
         * the program's current locale
         *
         * The isdigit() function shall return non-zero if c is a decimal digit
         */
        REQ("isdigit.01.01", "shall return non-zero", isdigit_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("digit"), c))
        {
        /*
         * The isdigit() function shall test whether c is a character of class digit in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isdigit.01.02", "shall return 0", isdigit_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isgraph - test for a visible character

SYNOPSIS

    #include <ctype.h>

    int isgraph(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isgraph() function shall test whether c is a character of class graph in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isgraph() function shall return non-zero if c is a character with a
    visible representation; otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isgraph_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isgraph.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("graph"), c))
        {
        /*
         * The isgraph() function shall test whether c is a character of class graph in
         * the program's current locale
         *
         * The isgraph() function shall return non-zero if c is a character with a visible
         * representation
         */
        REQ("isgraph.01.01", "shall return non-zero", isgraph_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("graph"), c))
        {
        /*
         * The isgraph() function shall test whether c is a character of class graph in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isgraph.01.02", "shall return 0", isgraph_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    islower - test for a lowercase letter

SYNOPSIS

    #include <ctype.h>

    int islower(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The islower() function shall test whether c is a character of class lower in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The islower() function shall return non-zero if c is a lowercase letter;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT islower_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.islower.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("lower"), c))
        {
        /*
         * The islower() function shall test whether c is a character of class lower in
         * the program's current locale
         *
         * The islower() function shall return non-zero if c is a lowercase letter
         */
        REQ("islower.01.01", "shall return non-zero", islower_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("lower"), c))
        {
        /*
         * The islower() function shall test whether c is a character of class lower in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("islower.01.02", "shall return 0", islower_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isprint - test for a printable character

SYNOPSIS

    #include <ctype.h>

    int isprint(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isprint() function shall test whether c is a character of class print in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isprint() function shall return non-zero if c is a printable character;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isprint_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isprint.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("print"), c))
        {
        /*
         * The isprint() function shall test whether c is a character of class print in
         * the program's current locale
         *
         * The isprint() function shall return non-zero if c is a printable character
         */
        REQ("isprint.01.01", "shall return non-zero", isprint_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("print"), c))
        {
        /*
         * The isprint() function shall test whether c is a character of class print in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isprint.01.02", "shall return 0", isprint_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    ispunct - test for a punctuation character

SYNOPSIS

    #include <ctype.h>

    int ispunct(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The ispunct() function shall test whether c is a character of class punct in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The ispunct() function shall return non-zero if c is a punctuation
    character; otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT ispunct_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.ispunct.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("punct"), c))
        {
        /*
         * The ispunct() function shall test whether c is a character of class punct in
         * the program's current locale
         *
         * The ispunct() function shall return non-zero if c is a punctuation character
         */
        REQ("ispunct.01.01", "shall return non-zero", ispunct_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("punct"), c))
        {
        /*
         * The ispunct() function shall test whether c is a character of class punct in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("ispunct.01.02", "shall return 0", ispunct_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isspace - test for a white-space character

SYNOPSIS

    #include <ctype.h>

    int isspace(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isspace() function shall test whether c is a character of class space in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isspace() function shall return non-zero if c is a white-space
    character; otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isspace_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isspace.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("space"), c))
        {
        /*
         * The isspace() function shall test whether c is a character of class space in
         * the program's current locale
         *
         * The isspace() function shall return non-zero if c is a white-space character
         */
        REQ("isspace.01.01", "shall return non-zero", isspace_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("space"), c))
        {
        /*
         * The isspace() function shall test whether c is a character of class space in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isspace.01.02", "shall return 0", isspace_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isupper - test for an uppercase letter

SYNOPSIS

    #include <ctype.h>

    int isupper(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isupper() function shall test whether c is a character of class upper in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isupper() function shall return non-zero if c is an uppercase letter;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isupper_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isupper.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("upper"), c))
        {
        /*
         * The isupper() function shall test whether c is a character of class upper in
         * the program's current locale
         *
         * The isupper() function shall return non-zero if c is an uppercase letter
         */
        REQ("isupper.01.01", "shall return non-zero", isupper_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("upper"), c))
        {
        /*
         * The isupper() function shall test whether c is a character of class upper in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isupper.01.02", "shall return 0", isupper_spec == 0);
        }

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    isxdigit - test for a hexadecimal digit

SYNOPSIS

    #include <ctype.h>

    int isxdigit(int c);

DESCRIPTION

    The functionality described on this reference page is aligned with the ISO
    C standard. Any conflict between the requirements described here and the ISO
    C standard is unintentional. This volume of IEEE Std 1003.1-2001 defers to
    the ISO C standard.

    The isxdigit() function shall test whether c is a character of class xdigit in
    the program's current locale; see the Base Definitions volume of IEEE Std
    1003. 1-2001, Chapter 7, Locale.

    The c argument is an int, the value of which the application shall ensure is
    a character representable as an unsigned char or equal to the value of the
    macro EOF. If the argument has any other value, the behavior is undefined.

RETURN VALUE

    The isxdigit() function shall return non-zero if c is a hexadecimal digit;
    otherwise, it shall return 0.

ERRORS

    No errors are defined.

*/

specification
IntT isxdigit_spec(CallContext context, IntT c)
{
    pre
    {
        /*
         * The c argument is an int, the value of which the application shall ensure is a
         * character representable as an unsigned char or equal to the value of the macro
         * EOF. If the argument has any other value, the behavior is undefined.
         */
        REQ("app.isxdigit.02", "c should be an unsigned char or EOF", ((c >= 0) && (c <= max_UCharT)) || (c == SUT_EOF));

        return true;
    }
    post
    {
        if(isInCTypeGroup_Locale(context, _CS("xdigit"), c))
        {
        /*
         * The isxdigit() function shall test whether c is a character of class xdigit in
         * the program's current locale
         *
         * The isxdigit() function shall return non-zero if c is a hexadecimal digit
         */
        REQ("isxdigit.01.01", "shall return non-zero", isxdigit_spec > 0);
        }
        else if(isNotInCTypeGroup_Locale(context, _CS("xdigit"), c))
        {
        /*
         * The isxdigit() function shall test whether c is a character of class xdigit in
         * the program's current locale
         *
         * otherwise, it shall return 0.
         */
        REQ("isxdigit.01.02", "shall return 0", isxdigit_spec == 0);
        }

        return true;
    }
}


/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

LocSpec* create_LocSpec()
{
    return create_List(&type_UShortTObj);
}

