/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PROCESS_ENVIRON_MODEL_SEH
#define PROCESS_ENVIRON_MODEL_SEH

#include "common/common_model.seh"
#include "process/process/process_model.seh"
#include "data/errno_model.seh"
#include "config/interpretation.seh"
#include "process/environ/environ_config.h"

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** getenv_spec **/
specification
CString * getenv_spec( CallContext context, CString * name )
   reads name
;

/** putenv_spec **/
specification
IntT putenv_spec( CallContext context, CString * string, ErrorCode * errno )
   reads string
;

/** setenv_spec **/
specification
IntT setenv_spec( CallContext context, CString * envname, CString * envval, IntT overwrite, ErrorCode * errno )
   reads  envname, envval
   writes * errno
;

/** unsetenv_spec **/
specification
IntT unsetenv_spec( CallContext context, CString * name, ErrorCode * errno )
   reads  name
   writes * errno
;

#endif
