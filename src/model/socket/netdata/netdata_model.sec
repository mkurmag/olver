/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Portions of this text are reprinted and reproduced in electronic form
 * from IEEE Std 1003.1, 2004 Edition, Standard for Information Technology
 * -- Portable Operating System Interface (POSIX), The Open Group Base
 * Specifications Issue 6, Copyright (C) 2001-2004 by the Institute of
 * Electrical and Electronics Engineers, Inc and The Open Group. In the
 * event of any discrepancy between this version and the original IEEE and
 * The Open Group Standard, the original IEEE and The Open Group Standard
 * is the referee document. The original Standard can be obtained online at
 * http://www.opengroup.org/unix/online.html.
 */


#include "socket/netdata/netdata_model.seh"


#pragma SEC subsystem netdata "socket.netdata"



/*
   The group of functions 'socket.netdata' consists of:
       htonl [1]
       htons [1]
       ntohl [1]
       ntohs [1]
 */

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    htonl, htons, ntohl, ntohs - convert values between host and network
    byte order

SYNOPSIS

    #include <arpa/inet.h>

    uint32_t htonl(uint32_t hostlong); uint16_t htons(uint16_t hostshort);
    uint32_t ntohl(uint32_t netlong); uint16_t ntohs(uint16_t netshort);

DESCRIPTION

    These functions shall convert 16-bit and 32-bit quantities between
    network byte order and host byte order.

    On some implementations, these functions are defined as macros.

    The uint32_t and uint16_t types are defined in <inttypes.h>.

RETURN VALUE

    The htonl() and htons() functions shall return the argument
    value converted from host to network byte order.

    The ntohl() and ntohs() functions shall return the argument
    value converted from network to host byte order.

ERRORS

    No errors are defined.
*/

specification
NetworkOrder4Bytes* htonl_spec( CallContext context, UInt32T hostlong)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ConvertValueFromHostToNetworkByteOrder, "Convert value from host to network byte order" };
    }
    post
    {
        /*
         * These functions shall convert 16-bit and 32-bit quantities
         * between network byte order and host byte order.
         */
        /*
         * On some implementations, these functions are defined as macros.
         */
        /*
         * The uint32_t and uint16_t types are defined in <inttypes.h>.
         */
        IMPLEMENT_REQ("htonl.01;htonl.02;htonl.03");

        /*
         * The htonl() and htons() functions shall return the argument
         * value converted from host to network byte order.
         */
        REQ("htonl.04",
            "Shall return the argument converted from host to network order",
            htonl_04(htonl_spec, hostlong));

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    htonl, htons, ntohl, ntohs - convert values between host and network
    byte order

SYNOPSIS

    #include <arpa/inet.h>

    uint32_t htonl(uint32_t hostlong); uint16_t htons(uint16_t hostshort);
    uint32_t ntohl(uint32_t netlong); uint16_t ntohs(uint16_t netshort);

DESCRIPTION

    These functions shall convert 16-bit and 32-bit quantities between
    network byte order and host byte order.

    On some implementations, these functions are defined as macros.

    The uint32_t and uint16_t types are defined in <inttypes.h>.

RETURN VALUE

    The htonl() and htons() functions shall return the argument
    value converted from host to network byte order.

    The ntohl() and ntohs() functions shall return the argument
    value converted from network to host byte order.

ERRORS

    No errors are defined.
*/
specification
NetworkOrder2Bytes* htons_spec( CallContext context, UInt16T hostshort)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ConvertValueFromHostToNetworkByteOrder, "Convert value from host to network byte order" };
    }
    post
    {
        /*
         * These functions shall convert 16-bit and 32-bit quantities
         * between network byte order and host byte order.
         */
        /*
         * On some implementations, these functions are defined as macros.
         */
        /*
         * The uint32_t and uint16_t types are defined in <inttypes.h>.
         */
        IMPLEMENT_REQ("htons.01;htons.02;htons.03");

        /*
         * The htonl() and htons() functions shall return the argument
         * value converted from host to network byte order.
         */
        REQ("htons.04",
            "Shall return the argument converted from host to network order",
            htons_04(htons_spec, hostshort));

        return true;
    }
}


/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    htonl, htons, ntohl, ntohs - convert values between host and network
    byte order

SYNOPSIS

    #include <arpa/inet.h>

    uint32_t htonl(uint32_t hostlong); uint16_t htons(uint16_t hostshort);
    uint32_t ntohl(uint32_t netlong); uint16_t ntohs(uint16_t netshort);

DESCRIPTION

    These functions shall convert 16-bit and 32-bit quantities between
    network byte order and host byte order.

    On some implementations, these functions are defined as macros.

    The uint32_t and uint16_t types are defined in <inttypes.h>.

RETURN VALUE

    The htonl() and htons() functions shall return the argument
    value converted from host to network byte order.

    The ntohl() and ntohs() functions shall return the argument
    value converted from network to host byte order.

ERRORS

    No errors are defined.
*/
specification
UInt32T ntohl_spec( CallContext context, NetworkOrder4Bytes* netlong)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ConvertValueFromNetworkToHostByteOrder, "Convert value from network to host byte order" };
    }
    post
    {
        /*
         * These functions shall convert 16-bit and 32-bit quantities
         * between network byte order and host byte order.
         */
        /*
         * On some implementations, these functions are defined as macros.
         */
        /*
         * The uint32_t and uint16_t types are defined in <inttypes.h>.
         */
        /*
         * The ntohl() and ntohs() functions shall return the argument
         * value converted from network to host byte order.
         */
        IMPLEMENT_REQ("ntohl.01;ntohl.02;ntohl.03;ntohl.04");

        return true;
    }
}

bool htonl_04(NetworkOrder4Bytes* htonl_spec, UInt32T hostlong)
{
    return  (unsigned char)htonl_spec->arr[3]==hostlong%256
            &&
            (unsigned char)htonl_spec->arr[2]==(hostlong>>8)%256
            &&
            (unsigned char)htonl_spec->arr[1]==(hostlong>>16)%256
            &&
            (unsigned char)htonl_spec->arr[0]==(hostlong>>24)%256;
}

/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

The Open Group Base Specifications Issue 6
IEEE Std 1003.1, 2004 Edition
Copyright (c) 2001-2004 The IEEE and The Open Group, All Rights reserved.

NAME

    htonl, htons, ntohl, ntohs - convert values between host and network
    byte order

SYNOPSIS

    #include <arpa/inet.h>

    uint32_t htonl(uint32_t hostlong); uint16_t htons(uint16_t hostshort);
    uint32_t ntohl(uint32_t netlong); uint16_t ntohs(uint16_t netshort);

DESCRIPTION

    These functions shall convert 16-bit and 32-bit quantities between
    network byte order and host byte order.

    On some implementations, these functions are defined as macros.

    The uint32_t and uint16_t types are defined in <inttypes.h>.

RETURN VALUE

    The htonl() and htons() functions shall return the argument
    value converted from host to network byte order.

    The ntohl() and ntohs() functions shall return the argument
    value converted from network to host byte order.

ERRORS

    No errors are defined.
*/
specification
UInt16T ntohs_spec( CallContext context, NetworkOrder2Bytes* netshort)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return { ConvertValueFromNetworkToHostByteOrder, "Convert value from network to host byte order" };
    }
    post
    {
        /*
         * These functions shall convert 16-bit and 32-bit quantities
         * between network byte order and host byte order.
         */
        /*
         * On some implementations, these functions are defined as macros.
         */
        /*
         * The uint32_t and uint16_t types are defined in <inttypes.h>.
         */
        /*
         * The ntohl() and ntohs() functions shall return the argument
         * value converted from network to host byte order.
         */
        IMPLEMENT_REQ("ntohs.01;ntohs.02;ntohs.03;ntohs.04");

        return true;
    }
}

bool htons_04(NetworkOrder2Bytes* htons_spec, UInt16T hostshort)
{
    return  (unsigned char)htons_spec->arr[1]==hostshort%256
            &&
            (unsigned char)htons_spec->arr[0]==(hostshort>>8)%256;
}

/********************************************************************/
/**                       Netdata Types                            **/
/********************************************************************/

specification typedef struct NetworkOrder2Bytes NetworkOrder2Bytes = {};
specification typedef struct NetworkOrder4Bytes NetworkOrder4Bytes = {};

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/
NetworkOrder4Bytes* create_NetworkOrder4Bytes(CharT first, CharT second,
                                                CharT third, CharT fourth)
{
    CharT arr[4];
    arr[0]=first;
    arr[1]=second;
    arr[2]=third;
    arr[3]=fourth;

    return create(&type_NetworkOrder4Bytes, arr);
}
NetworkOrder2Bytes* create_NetworkOrder2Bytes(CharT first, CharT second)
{
    CharT arr[2];
    arr[0]=first;
    arr[1]=second;

    return create(&type_NetworkOrder2Bytes, arr);
}

UInt16T htons_model(UInt16T hostshort)
{
    UInt16T res;
    UInt8T * ph = (UInt8T *)&hostshort;
    UInt8T * pn = (UInt8T *)&res;
    if(__BYTE_ORDER == __LITTLE_ENDIAN)
    {
    	pn[0] = ph[1];
	pn[1] = ph[0];
    }
    else
    {  
    	pn[0] = ph[0];
	pn[1] = ph[1];
    }
    return res;
}

UInt16T ntohs_model(UInt16T netshort)
{
    UInt16T res;
    UInt8T * ph = (UInt8T *)&netshort;
    UInt8T * pn = (UInt8T *)&res;
    if(__BYTE_ORDER == __LITTLE_ENDIAN)
    {
    	pn[0] = ph[1];
	pn[1] = ph[0];
    }
    else
    {  
    	pn[0] = ph[0];
	pn[1] = ph[1];
    }
    return res;
}

UInt32T htonl_model(UInt32T hostshort)
{
    UInt32T res;
    UInt8T * ph = (UInt8T *)&hostshort;
    UInt8T * pn = (UInt8T *)&res;
    if(__BYTE_ORDER == __LITTLE_ENDIAN)
    {
        pn[0] = ph[3];
        pn[1] = ph[2];
        pn[2] = ph[1];
        pn[3] = ph[0];
    }
    else
    {
        pn[0] = ph[0];
        pn[1] = ph[1];
        pn[2] = ph[2];
        pn[3] = ph[3];
    }
    return res;
}

UInt32T ntohl_model(UInt32T netshort)
{
    UInt32T res;
    UInt8T * ph = (UInt8T *)&netshort;
    UInt8T * pn = (UInt8T *)&res;
    if(__BYTE_ORDER == __LITTLE_ENDIAN)
    {
        pn[0] = ph[3];
        pn[1] = ph[2];
        pn[2] = ph[1];
        pn[3] = ph[0];
    }
    else
    {
        pn[0] = ph[0];
        pn[1] = ph[1];
        pn[2] = ph[2];
        pn[3] = ph[3];
    }
    return res;
}
