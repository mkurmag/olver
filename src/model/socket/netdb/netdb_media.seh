/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef SOCKET_NETDB_MEDIA_SEH
#define SOCKET_NETDB_MEDIA_SEH

#include "socket/netdb/netdb_model.seh"

AddrInfoT * create_AddrInfoT_VoidTPtr(CallContext context, AddrInfoTPtr ptr);

AddrInfoT * create_AddrInfoT(CallContext context, 
                             IntT ai_flags, IntT ai_family, IntT ai_socktype, IntT ai_protocol,
                             sut_socklen_t ai_addrlen, SockaddrT * ai_addr,
                             CString * ai_canonname, AddrInfoTPtr ai_next);

AddrInfoT * create_emptyAddrInfoT(CallContext context);

ServentT * readServentT_TSStream(ThreadId threadid,TSStream* stream);
ProtoentT * readProtoentT_TSStream(ThreadId threadid,TSStream* stream);
HostentT * readHostentT_TSStream(ThreadId threadid,TSStream* stream);

/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initSocketNetdbSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** endservent_spec **/
mediator endservent_media for specification
void endservent_spec( CallContext context);

/** getservbyname_spec **/
mediator getservbyname_media for specification
ServentT * getservbyname_spec( CallContext context, CString *name, CString *proto );

/** getservbyport_spec **/
mediator getservbyport_media for specification
ServentT * getservbyport_spec( CallContext context, IntT port, CString * proto );

/** getservent_spec **/
mediator getservent_media for specification
ServentT * getservent_spec( CallContext context);

/** setservent_spec **/
mediator setservent_media for specification
void setservent_spec( CallContext context, IntT stayopen);

/**/

/** endprotoent_spec **/
mediator endprotoent_media for specification
void endprotoent_spec( CallContext context);

/** getprotobyname_spec **/
mediator getprotobyname_media for specification
ProtoentT * getprotobyname_spec( CallContext context, CString * name);

/** getprotobynumber_spec **/
mediator getprotobynumber_media for specification
ProtoentT * getprotobynumber_spec( CallContext context, IntT proto );

/** getprotoent_spec **/
mediator getprotoent_media for specification
ProtoentT * getprotoent_spec( CallContext context );

/** setprotoent_spec **/
mediator setprotoent_media for specification
void setprotoent_spec( CallContext context, IntT stayopen);

/**/

/** freeaddrinfo_spec **/
mediator freeaddrinfo_media for specification
void freeaddrinfo_spec( CallContext context, AddrInfoTPtr ai_ptr );

/** getaddrinfo_spec **/
mediator getaddrinfo_media for specification
IntT getaddrinfo_spec( CallContext context, CString * nodename, 
                       CString * servname, AddrInfoT * hints, VoidTPtrObj * res );
/**/

/** getnameinfo_spec **/
mediator getnameinfo_media for specification
IntT getnameinfo_spec( CallContext context, SockaddrT * sa, 
                       CString * node, sut_socklen_t nodelen, 
                       CString * service, sut_socklen_t servicelen, 
                       IntT flags );

/**/

/** gethostbyaddr_spec **/
mediator gethostbyaddr_media for specification
HostentT * gethostbyaddr_spec( CallContext context, VoidTPtr addr, sut_socklen_t len, IntT type );

/** gethostbyname_spec **/
mediator gethostbyname_media for specification
HostentT * gethostbyname_spec( CallContext context, CString * name );

/**/


/** gai_strerror_spec **/
mediator gai_strerror_media for specification
CString * gai_strerror_spec( CallContext context, IntT ecode );

/** __h_errno_location_spec **/
mediator __h_errno_location_media for specification
VoidTPtr __h_errno_location_spec( CallContext context);


#endif

