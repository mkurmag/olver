/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "socket/rpc/pmap_media.seh"
#include "socket/rpc/pmap_model.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initSocketRpcPmapSubsystem(void)
{
    if(!init_pmap())
    {
        traceUserInfo("init_pmap failed!");
        return;
    }

  // Set up mediators
    set_mediator_pmap_getport_spec(pmap_getport_media);
    set_mediator_pmap_set_spec(pmap_set_media);
    set_mediator_pmap_unset_spec(pmap_unset_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** pmap_getport_spec **/
mediator pmap_getport_media for specification
UShortT pmap_getport_spec( CallContext context, SockaddrT * addr, 
                          ULongT prognum, ULongT versnum, ULongT protocol )

{
    call
    {
        TSCommand command = create_TSCommand();
        UShortT res;
    
        format_TSCommand( &command, "pmap_getport:$(sockaddr)$(ulong)$(ulong)$(ulong)", addr,
                    create_ULongTObj(prognum), create_ULongTObj(versnum), create_ULongTObj(protocol) 
                   );
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUShort_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
}


/** pmap_set_spec **/
mediator pmap_set_media for specification
BoolT pmap_set_spec( CallContext context, ULongT program, ULongT version, IntT protocol, UShortT port)
{
    call
    {
        TSCommand command = create_TSCommand();
        BoolT res;
    
        format_TSCommand( &command, "pmap_set:$(ulong)$(ulong)$(int)$(ushort)", 
                          create_ULongTObj(program), create_ULongTObj(version), 
                          create_IntTObj(protocol), create_UShortTObj(port) 
                        );
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        onPmapSet(context, program, version, protocol, port, pmap_set_spec);
    }
}


/** pmap_unset_spec **/
mediator pmap_unset_media for specification
BoolT pmap_unset_spec( CallContext context, ULongT prognum, ULongT versnum )
{
    call
    {
        TSCommand command = create_TSCommand();
        BoolT res;

        format_TSCommand( &command, "pmap_unset:$(ulong)$(ulong)",
                          create_ULongTObj(prognum), create_ULongTObj(versnum)
                        );
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res; 
    }
    state
    {
        onPmapUnset(context, prognum, versnum, pmap_unset_spec);
    }
} 



