/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Portions of this text are reprinted and reproduced in electronic form
 * from IEEE Std 1003.1, 2004 Edition, Standard for Information Technology
 * -- Portable Operating System Interface (POSIX), The Open Group Base
 * Specifications Issue 6, Copyright (C) 2001-2004 by the Institute of
 * Electrical and Electronics Engineers, Inc and The Open Group. In the
 * event of any discrepancy between this version and the original IEEE and
 * The Open Group Standard, the original IEEE and The Open Group Standard
 * is the referee document. The original Standard can be obtained online at
 * http://www.opengroup.org/unix/online.html.
 */


#include "socket/rpc/clnt_model.seh"


#pragma SEC subsystem rpc "socket.rpc"



/*
   The group of functions 'socket.rpc.clnt' consists of:
       authnone_create [1]
       clnt_create [1]
       clnt_pcreateerror [1]
       clnt_perrno [1]
       clnt_perror [1]
       clnt_spcreateerror [1]
       clnt_sperrno [1]
       clnt_sperror [1]
 */

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

    refers

System V Interface Definition, Fourth Edition
FINAL COPY June 15, 1995
Copyright (c) 1983, 1984, 1985, 1986,1987, 1988, 1995 Novell, Inc.

SYNOPSIS

    #include <rpc/rpc.h>

    extern struct AUTH *authnone_create(void);
    extern struct CLIENT *clnt_create(const char *, const u_long, const u_long, const char *);
    extern void clnt_pcreateerror(const char *);
    extern void clnt_perrno(enum clnt_stat);
    extern void clnt_perror(struct CLIENT *, const char *);
    extern char *clnt_spcreateerror(const char *);
    extern char *clnt_sperrno(enum clnt_stat);
    extern char *clnt_sperror(struct CLIENT *, const char *);

    #define clnt_control(cl,rq,in)((*(cl)->cl_ops->cl_control)(cl,rq,in))
    #define clnt_abort(rh)((*(rh)->cl_ops->cl_abort)(rh))
    #define clnt_destroy(rh)((*(rh)->cl_ops->cl_destroy)(rh))
    #define clnt_freeres(rh,xres,resp)((*(rh)->cl_ops->cl_freeres)(rh,xres,resp))
    #define clnt_geterr(rh,errp)((*(rh)->cl_ops->cl_geterr)(rh, errp))
    #define clnt_call(rh, proc, xargs, argsp, xres, resp, secs)\
    ((*(rh)->cl_ops->cl_call)(rh, proc, xargs, argsp, xres, resp, secs))

DESCRIPTION

    AUTH *
    authnone_create(void);

        Create and return an RPC authentication handle that passes nonusable
        authentication information with each remote procedure call. This is the
        default authentication used by RPC.

    CLIENT *
    clnt_create(const char *host, const u_long prognum,
    const u_long versnum, const char *nettype);

        Generic client creation routine. host identifies the name of the remote host
        where the server is located. nettype indicates the class of transport protocol
        to use. The transports are tried in left to right order in NETPATH variable or
        in top to down order in the netconfig database.
        clnt_create() tries all the transports of the nettype class available from the
        NETPATH environment variable and the the netconfig database, and chooses the
        first successful one. Default timeouts are set, but can be modified using
        clnt_control().

    void
    clnt_pcreateerror(const char *s);

        Print a message to standard error indicating why a client RPC handle could not
        be created. The message is prepended with the string s and a colon, and
        appended with a NEWLINE.

    void
    clnt_perrno(const enum clnt_stat stat);

        Print a message to standard error corresponding to the condition indicated by
        stat. A NEWLINE is appended at the end of the message. Normally used after a
        procedure call fails, for instance rpc_call().

    void
    clnt_perror(const CLIENT *clnt, const char *s);

        Print a message to standard error indicating why an RPC call failed; clnt is
        the handle used to do the call. The message is prepended with string s and a
        colon. A NEWLINE is appended at the end of the message. Normally used after a
        procedure call fails, for instance clnt_call().

    char *
    clnt_spcreateerror(const char *s);

        Like clnt_pcreateerror(), except that it returns a string instead of printing
        to the standard error. A NEWLINE is not appended to the message in this case.
        Warning: returns a pointer to static data that is overwritten on each call.

    char *
    clnt_sperrno(const enum clnt_stat stat);

        Take the same arguments as clnt_perrno(), but instead of sending a message to
        the standard error indicating why an RPC call failed, return a pointer to a
        string which contains the message.
        clnt_sperrno() is normally used instead of clnt_perrno() when the program does
        not have a standard error (as a program running as a server quite likely does
        not), or if the programmer does not want the message to be output with printf()
        [see printf(BA_LIB)], or if a message format different than that supported by
        clnt_perrno() is to be used. Note: unlike clnt_sperror() and
        clnt_spcreaterror() [see rpc_clnt_create(RS_LIB)], clnt_sperrno() does not
        return pointer to static data so the result will not get overwritten on each
        call.

    char *
    clnt_sperror(const CLIENT *clnt, const char *s);

        Like clnt_perror(), except that (like clnt_sperrno()) it returns a string
        instead of printing to standard error. However, clnt_sperror() does not append
        a NEWLINE at the end of the message.
        Warning: returns pointer to static data that is overwritten on each call.

    bool_t
    clnt_control(CLIENT *clnt, const u_int req, char *info);

        A function macro used to change or retrieve various information about a client
        object. req indicates the type of operation, and info is a pointer to the
        information. For both connectionless and connection-oriented transports, the
        supported values of req and their argument types and what they do are:
        CLSET_TIMEOUT   struct timeval  set total timeout
        CLGET_TIMEOUT   struct timeval  get total timeout
        Note: if you set the timeout using clnt_control(), the timeout parameter passed
        to clnt_call() will be ignored in all future calls.
        CLGET_FD        int             get the associated file descriptor
        CLGET_SVC_ADDR  struct netbuf   get servers address
        CLSET_FD_CLOSE  int             close the file descriptor when destroying the
                                        client handle [see clnt_destroy()]
        CLSET_FD_NCLOSE int             do not close the file descriptor when destroying
                                        the client handle
        The following operations are valid for connectionless transports only:
        CLSET_RETRY_TIMEOUT     struct timeval  set the retry timeout
        CLGET_RETRY_TIMEOUT     struct timeval  get the retry timeout
        The retry timeout is the time that RPC waits for the server to reply before
        retransmitting the request.
        clnt_control() returns 1 on success and 0 on failure.

    void
    clnt_destroy(CLIENT *clnt);

        A function macro that destroys the client�s RPC handle. Destruction usually
        involves deallocation of private data structures, including clnt itself. Use of
        clnt is undefined after calling clnt_destroy(). If the RPC library opened the
        associated file descriptor, or CLSET_FD_CLOSE was set using clnt_control(), it
        will be closed.

    bool_t clnt_freeres(CLIENT *clnt, const xdrproc_t outproc, caddr_t out);

        A function macro that frees any data allocated by the RPC/XDR system when it
        decoded the results of an RPC call. The parameter out is the address of the
        results, and outproc is the XDR routine describing the results. This routine
        returns 1 if the results were successfully freed, and 0 otherwise.

    void
    clnt_geterr(const CLIENT *clnt, struct rpc_err *errp);

        A function macro that copies the error structure out of the client handle to
        the structure at address errp.

    enum clnt_stat
    clnt_call(CLIENT *clnt, const u_long procnum, const xdrproc_t inproc,
    caddr_t in, const xdrproc_t outproc, caddr_t out,
    const struct timeval tout);

        A function macro that calls the remote procedure procnum associated with the
        client handle, clnt, which is obtained with an RPC client creation routine
        such as clnt_create() [see rpc_clnt_create(RS_LIB)]. The parameter in is the
        address of the procedure�s argument(s), and out is the address of where to
        place the result(s); inproc is used to encode the procedure�s parameters, and
        outproc is used to decode the procedure�s results; tout is the time allowed
        for results to be returned.
        If the remote call succeeds, the status is returned in RPC_SUCCESS, otherwise
        an appropriate status is returned [see the Remote Services Definitions chapter
        for possible error numbers].

*/

specification
AuthTPtr authnone_create_spec(CallContext context)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
       /*
         * Create and return an RPC authentication handle that passes nonusable
         * authentication information with each remote procedure call.
         */
        REQ("authnone_create.01", "", TODO_REQ());

        return true;
    }
}


specification
ClientTPtr clnt_create_spec(CallContext context, CString* host, ULongT prognum,
                            ULongT versnum, CString* nettype)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        /*
         * Generic client creation routine.
         */
        REQ("clnt_create.01", "", TODO_REQ());

        /*
         * clnt_create() tries all the transports of the nettype class available from the
         * NETPATH environment variable and the the netconfig database, and chooses the
         * first successful one.
         */
        REQ("clnt_create.02", "", TODO_REQ());
        
        return true;
    }
}


specification
void clnt_pcreateerror_spec(CallContext context, CString* s)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
       /*
         * Print a message to standard error indicating why a client RPC handle could not
         * be created.
         */
        REQ("clnt_pcreateerror.01", "", TODO_REQ());

        /*
         * The message is prepended with the string s and a colon, and appended with a
         * NEWLINE.
         */
        REQ("clnt_pcreateerror.02", "", TODO_REQ());

        return true;
    }
}


specification
void clnt_perrno_spec(CallContext context, enum ClntStatT stat)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
       /*
         * Print a message to standard error corresponding to the condition indicated by
         * stat.
         */
        REQ("clnt_perrno.01", "", TODO_REQ());

        /*
         * A NEWLINE is appended at the end of the message.
         */
        REQ("clnt_perrno.02", "", TODO_REQ());
        
        return true;
    }
}


specification
void clnt_perror_spec(CallContext context, ClientTPtr clnt, CString* s)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
       /*
         * Print a message to standard error indicating why an RPC call failed;
         */
        REQ("clnt_perror.01", "", TODO_REQ());

        /*
         * The message is prepended with string s and a colon. A NEWLINE is appended at
         * the end of the message.
         */
        REQ("clnt_perror.02", "", TODO_REQ());

        return true;
    }
}


specification
CString* clnt_spcreateerror_spec(CallContext context, CString* s)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        /*
         * Like clnt_pcreateerror(), except that it returns a string instead of printing
         * to the standard error.
         */
        REQ("clnt_spcreateerror.01", "", TODO_REQ());

        /*
         * A NEWLINE is not appended to the message in this case.
         */
        REQ("clnt_spcreateerror.02", "", TODO_REQ());

        /*
         * unlike clnt_sperror() and clnt_spcreaterror() [see rpc_clnt_create(RS_LIB)],
         * clnt_sperrno() does not return pointer to static data so the result will not
         * get overwritten on each call.
         */
        REQ("clnt_spcreateerror.03", "", TODO_REQ());

        return true;
    }
}


specification
CString* clnt_sperrno_spec(CallContext context, enum ClntStatT stat)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        /*
         * Take the same arguments as clnt_perrno(), but instead of sending a message to
         * the standard error indicating why an RPC call failed, return a pointer to a
         * string which contains the message.
         */
        REQ("clnt_sperrno.01", "", TODO_REQ());

        /*
         * unlike clnt_sperror() and clnt_spcreaterror() [see rpc_clnt_create(RS_LIB)],
         * clnt_sperrno() does not return pointer to static data so the result will not
         * get overwritten on each call.
         */
        REQ("clnt_sperrno.02", "", TODO_REQ());
        
        return true;
    }
}


specification
CString* clnt_sperror_spec(CallContext context, ClientTPtr clnt, CString* s)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
       /*
         * unlike clnt_sperror() and clnt_spcreaterror() [see rpc_clnt_create(RS_LIB)],
         * clnt_sperrno() does not return pointer to static data so the result will not
         * get overwritten on each call.
         */
        REQ("clnt_sperror.03", "", TODO_REQ());

        /*
         * Like clnt_perror(), except that (like clnt_sperrno()) it returns a string
         * instead of printing to standard error.
         */
        REQ("clnt_sperror.01", "", TODO_REQ());

        /*
         * However, clnt_sperror() does not append a NEWLINE at the end of the message.
         */
        REQ("clnt_sperror.02", "", TODO_REQ());

        return true;
    }
}


specification
BoolT clnt_control_spec(CallContext context, ClientTPtr clnt, UIntT req, VoidTPtr info)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        return true;
    }
}


specification
void clnt_destroy_spec(CallContext context, ClientTPtr clnt)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        return true;
    }
}


specification
BoolT clnt_freeres_spec(CallContext context, ClientTPtr clnt, XdrProcTPtr outproc, CaddrT out)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        return true;
    }
}


specification
void clnt_geterr_spec(CallContext context, ClientTPtr clnt, RpcErrTPtr errp)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        return true;
    }
}


specification
enum ClntStatT clnt_call_spec(CallContext context, ClientTPtr clnt, ULongT procnum, XdrProcTPtr inproc,
                              CaddrT in, XdrProcTPtr outproc, CaddrT out, TimeValTObj* tout)
{
    pre
    {
        return true;
    }
    coverage C
    {
        return {TheOnlyBranch, "The only branch"};
    }
    post
    {
        return true;
    }
}


/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/
