/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "system/user/tests/account_scenario.seh"

#include "common/common_media.seh"
#include "common/common_scenario.seh"

#include "config/system_config.seh"
#include "process/meta/meta_model.seh"
#include "system/user/tests/account_scenario.seh"
#include "system/user/account_model.seh"
#include "system/user/account_media.seh"

#include "io/file/file_model.seh"

/********************************************************************/
/**                       Test Scenario Data                       **/
/********************************************************************/
static CallContext context;

static VoidTPtr ptr;
VoidTPtr addr;
//VoidTPtr addr2;
CString * ut_line;
CString * ut_id;
CString * ut_user;
CString * ut_host;
CString * dbname;
PidT ut_pid;
TimeVal * ut_tv;

//UtmpT * utmp;
UtmpxT* utmpx;

/********************************************************************/
/**                  Test Scenario Initialization                  **/
/********************************************************************/

static bool init_account_scenario(int argc, char** argv)
{
    /* Init test scenario data */
    context = getContext();

    // make sure process has appropriate priviledges
    INIT_SCENARIO_ACTION( makeSureProcessHasAppropriatePrivileges(context) );
    // update the current working directory of the process
    INIT_SCENARIO_ACTION( updateProcessWorkDirectory(context) );
    // prepare test data dir
    INIT_SCENARIO_ACTION( createTestScenarioSandboxDir(context) );
    
    dbname = getTestScenarioSandboxPathCh("utmp");
    close_spec(context, creat_spec(context, dbname, create_FilePermissions_String("rwxrwxrwx"), requestErrorCode(), false), requestErrorCode());
    
    addr = allocateMemoryBlock(context, 500);
    //addr2 = allocateMemoryBlock(context, 500);

    ptr = allocateMemoryBlock( context, sizeof_Type(context, "void*") );
    ut_id = create_CString( "olvr" );
    ut_line = create_CString( "/tmp/olver/device" );
    ut_user = create_CString( "olver_tester" );
    ut_host = create_CString( "localhost" );
    ut_pid = getpid_spec(context).process;
    ut_tv = create_TimeVal(100000, 100);
	
	utmpx = create_UtmpxT(addr, SUT_USER_PROCESS, ut_pid, ut_line, ut_id, ut_user, ut_host, ut_tv);
    // should be better created as utmp = logwtmp_spec( context, ut_line, ut_user, ut_host );
    // switch ut_type to SUT_DEAD_PROCESS in order to remove line from utmp file

    utmpname_spec(context, dbname);
	pututxline_spec(context, utmpx, requestErrorCode());
    
    return true;
}

static void finish_account_scenario(void)
{
    endutent_spec(context);
    utmpname_spec(context, create_CString("/var/run/utmp"));
    
    removeTestScenarioSandboxDir(context);
    
    deallocateMemoryBlock( context, ptr );
    deallocateMemoryBlock( context, addr );
}

/********************************************************************/
/**                          Test Actions                          **/
/********************************************************************/


scenario
bool endutxent_scen(void)
{
    endutxent_spec(context);

    return true;
}

scenario
bool getutxent_scen(void)
{
    setutxent_spec(context);
    getutxent_spec(context);

    return true;
}

scenario
bool getutxid_scen(void)
{
    setutxent_spec(context); // reset cursor
   	getutxid_spec(context, utmpx );

    return true;
}

scenario
bool getutxline_scen(void)
{
    setutxent_spec(context); // reset cursor
    getutxline_spec(context, utmpx );

    return true;
}

scenario
bool pututxline_scen(void)
{
    setutxent_spec(context); // reset cursor
    pututxline_spec(context, utmpx, requestErrorCode() );

    return true;
}

scenario
bool setutxent_scen(void)
{
    setutxent_spec(context);

    return true;
}

scenario
bool getutent_scen(void)
{
    UtmpT * utmp;
    setutent_spec(context);
    do{
        utmp = getutent_spec(context);
        if(utmp!=NULL)
            DUMP("getutent: %s\n",toCharArray_CString(utmp->ut_line));
    }while(utmp);
    return true;
}

scenario
bool getutent_r_scen(void)
{
    VoidTPtr addr2 = allocateMemoryBlock(context, 500);
    UtmpT * utmp = create_UtmpT(addr2, SUT_USER_PROCESS, ut_pid, ut_line, ut_id, ut_user, ut_host, ut_tv);
    
    setutent_spec(context); // reset cursor
    getutent_r_spec( context, utmp, ptr );

    deallocateMemoryBlock( context, addr2 );
    
    return true;
}

scenario
bool login_scen(void)
{
    UtmpT * utmp;
    IntT res;
    
    utmp = create_UtmpT(addr, SUT_USER_PROCESS, ut_pid, ut_line, ut_id, ut_user, ut_host, ut_tv);
    login_spec( context, utmp );
    
    utmpx->ut_id = utmp->ut_id;
    
    // we should get real line value after login in order to logout successfully
    // back to main db
    utmpname_spec(context, create_CString("/var/run/utmp"));
    setutent_spec(context);
    utmpx = getutxid_spec(context, utmpx);
    endutent_spec(context);
    utmpname_spec(context, dbname);
    
    utmp->ut_line = utmpx->ut_line;
    
    verbose("line: %s\n", toCharArray_CString(utmp->ut_line));
    res = logout_spec( context, utmp->ut_line );
    verbose("logout(1 on success): %d\n", res);
    
    return true;
}

scenario
bool logout_scen(void)
{
    // just stab, see login_scen
    logout_spec( context, ut_line );

    return true;
}

scenario
bool logwtmp_scen(void)
{
    logwtmp_spec( context, ut_line, ut_user, ut_host );

    return true;
}

scenario
bool setutent_scen(void)
{
    setutent_spec( context );

    return true;
}

scenario
bool utmpname_scen(void)
{
    utmpname_spec( context, dbname );

    return true;
}

scenario
bool endutent_scen(void)
{
    endutent_spec(context);

    return true;
}

scenario ndfsm system_user_account_scenario =
{
    .init = init_account_scenario,
    .finish = finish_account_scenario,

    .actions = {
        endutxent_scen,
        getutxent_scen,
        getutxid_scen,
        getutxline_scen,
        pututxline_scen,
        setutxent_scen,
        getutent_scen,
        getutent_r_scen,
        login_scen,
        logout_scen,
        logwtmp_scen,
        setutent_scen,
        utmpname_scen,
        endutent_scen,
        NULL
    }
};



#ifdef SYSTEM_USER_ACCOUNT_LOCAL_MAIN

#include "common/init.seh"

#include "common/common_media.seh"
#include "common/common_scenario.seh"
#include "common/control_center.seh"
#include "system/system/system_model.seh"


/********************************************************************/
/**                     Test System Initialization                 **/
/********************************************************************/
void reinitTestSystem(void)
{
    reinitControlCenter();
    initCommonModel();
    initCommonMedia();
    initCommonScenarioState();

    initSystemConfiguration();
    initSystemModel();

    initSystemUserAccountSubsystem();
}


bool main_system_user_account(int argc, char** argv)
{
    system_user_account_scenario(argc,argv);
    return true;
}


int main(int argc, char** argv)
{
    initTestSystem();
    loadSUT();

    /* Set up tracer */
    /*setTraceEncoding("windows-1251");
     */

    /* Run test scenario */
    main_system_user_account(argc,argv);

    /*  unloadSUT(); */
    return 0;
}
#endif
