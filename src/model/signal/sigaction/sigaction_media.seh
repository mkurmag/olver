/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef SIGNAL_SIGACTION_MEDIA_SEH
#define SIGNAL_SIGACTION_MEDIA_SEH

#include "signal/sigaction/sigaction_model.seh"

extern int worked_reactions;

/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initSignalSigactionSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** __libc_current_sigrtmax_spec **/
mediator __libc_current_sigrtmax_media for specification
IntT __libc_current_sigrtmax_spec( CallContext context);

/** __libc_current_sigrtmin_spec **/
mediator __libc_current_sigrtmin_media for specification
IntT __libc_current_sigrtmin_spec( CallContext context);

/** bsd_signal_spec **/
mediator bsd_signal_media for specification
SignalHandler bsd_signal_spec( CallContext context, Signal* sig, SignalHandler handler, ErrorCode* errno);

/** pause_spec **/
mediator pause_media for specification
void pause_spec( CallContext context);

mediator pause_return_media for reaction
PauseReturnType* pause_return(void);

/** pthread_sigmask_spec **/
//This mediator refers to: sigprocmask, pthread_sigmask
mediator pthread_sigmask_media for specification
void pthread_sigmask_spec( CallContext context, SigMaskAction how, SigSetTPtr set, SigSetTPtr oset, bool singleThreaded);

mediator pthread_sigmask_react_media for reaction
Pthread_SigmaskReactionType* pthread_sigmask_react(void);
/** sigaction_spec **/
mediator sigaction_media for specification
IntT sigaction_spec(CallContext context, Signal* sig, SigAction* s_act, 
                    SigAction* s_oact, ErrorCode* errno);

/** sigaltstack_spec **/
mediator sigaltstack_media for specification
SigaltstackReturnType* sigaltstack_spec( CallContext context, StackT* ss, ErrorCode* errno);

/** siginterrupt_spec **/
mediator siginterrupt_media for specification
IntT siginterrupt_spec( CallContext context, Signal* sig, IntT flag, ErrorCode* errno);

/** signal_spec **/
//This mediator refers to: __sysv_signal, signal
mediator signal_media for specification
SignalHandler signal_spec(CallContext context, Signal* sig, SignalHandler handler, 
                          ErrorCode* errno, bool lsbFunc);

/** sigpending_spec **/
mediator sigpending_media for specification
IntT sigpending_spec(CallContext context, SigSetTPtr set, ErrorCode* errno);

//mediator sigprocmask_react_media for reaction
//SigprocmaskReactionType* sigprocmask_react(void);
/** sigreturn_spec **/
//mediator sigreturn_media for specification
//ReturnType sigreturn_spec( CallContext context, ... );

/** sigsuspend_spec **/
mediator sigsuspend_media for specification
void sigsuspend_spec( CallContext context, SigSetTPtr sigmask);

mediator sigsuspend_return_media for
reaction SigsuspendReturnType* sigsuspend_return(void);

/************************************************************************/
/*                  Mediators for catchers reactions                    */
/************************************************************************/

mediator user_catcher_handler_reaction_start_media for 
reaction UserCatcherHandlerStartReactionType* user_catcher_handler_reaction_start(void);
mediator user_catcher_handler_reaction_finish_media for 
reaction UserCatcherHandlerFinishReactionType* user_catcher_handler_reaction_finish(void);

mediator user_agent_handler_reaction_start_media for 
reaction UserAgentHandlerStartReactionType* user_agent_handler_reaction_start(void);
mediator user_agent_handler_reaction_finish_media for 
reaction UserAgentHandlerFinishReactionType* user_agent_handler_reaction_finish(void);

mediator user_catcher_sigaction_reaction_start_media for 
reaction UserCatcherSigactionStartReactionType* user_catcher_sigaction_reaction_start(void);
mediator user_catcher_sigaction_reaction_finish_media for 
reaction UserCatcherSigactionFinishReactionType* user_catcher_sigaction_reaction_finish(void);

mediator user_agent_sigaction_reaction_start_media for 
reaction UserAgentSigactionStartReactionType* user_agent_sigaction_reaction_start(void);
mediator user_agent_sigaction_reaction_finish_media for 
reaction UserAgentSigactionFinishReactionType* user_agent_sigaction_reaction_finish(void);

/************************************************************************/
/*                  Formatters                                          */
/************************************************************************/
static void sigaction_data_formatter(TSStream* stream,SigAction* data);
void writeSigActionData_TSStream( TSStream* stream, SigAction* data);

/*
 * This function read realization state of sigaction struct and fill SigAction
 * "open state" fields to the return value
 */
void read_StructSigaction(SigAction* s_oact, TSStream* stream);

#endif

