/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef IO_FSTREAM_BUFFER_MEDIA_SEH
#define IO_FSTREAM_BUFFER_MEDIA_SEH

#include "io/fstream/buffer_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initIoFstreamBufferSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** setbuf_spec **/
mediator setbuf_media for specification
void setbuf_spec( CallContext context, FILETPtr stream, VoidTPtr buf );

/** setbuffer_spec **/
mediator setbuffer_media for specification
void setbuffer_spec( CallContext context, FILETPtr stream, VoidTPtr buf, SizeT size, ErrorCode * errno );

/** setvbuf_spec **/
mediator setvbuf_media for specification
IntT setvbuf_spec( CallContext context, FILETPtr stream, VoidTPtr buf, IntT type, SizeT size, ErrorCode * errno );

#endif
