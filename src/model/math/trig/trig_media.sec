 /*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "math/trig/trig_media.seh"
#include "math/math/math_config.h"
#include "common/common_media.seh"
#include "config/type_config.seh"
//#include <fenv.h>
#include <math.h>


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initMathTrigSubsystem(void)
{
  // Set up mediators
    set_mediator_acos_spec(acos_media);
    set_mediator_asin_spec(asin_media);
    set_mediator_atan_spec(atan_media);
    set_mediator_atan2_spec(atan2_media);
    set_mediator_cos_spec(cos_media);
    set_mediator_sin_spec(sin_media);
    set_mediator_sincos_spec(sincos_media);
    set_mediator_tan_spec(tan_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** acos_spec **/
//This mediator refers to: acosf, acos, acosl
mediator acos_media for 
specification
Unifloat* acos_spec(CallContext context, Unifloat* x, Unifloat* acosine, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        if ((x->type == UniDoubleT) && (CHECK_EXTENDED_REQ))
            format_TSCommand( &command, "acos_adv:$(unifloat)$(int)", x, create_IntTObj(rounding));        
        else
            switch(x->type) 
            {
            case 0: format_TSCommand( &command, "acosf:$(unifloat)", x);
                break;
            case 1: format_TSCommand( &command, "acos:$(unifloat)", x);
                break;
            case 2: format_TSCommand( &command, "acosl:$(unifloat)", x);
                break;
            }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** asin_spec **/
//This mediator refers to: asinf, asin, asinl
mediator asin_media for 
specification
Unifloat* asin_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
   
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "asinf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "asin:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "asinl:$(unifloat)", x);
            break;
        }

        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** atan_spec **/
//This mediator refers to: atanf, atan, atanl
mediator atan_media for 
specification
Unifloat* atan_spec(CallContext context, Unifloat* x, Unifloat* atangent, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        if ((x->type == UniDoubleT) && (CHECK_EXTENDED_REQ))
            format_TSCommand( &command, "atan_adv:$(unifloat)$(int)", x, create_IntTObj(rounding));        
        else
            switch(x->type) 
            {
            case 0: format_TSCommand( &command, "atanf:$(unifloat)", x);
                break;
            case 1: format_TSCommand( &command, "atan:$(unifloat)", x);
                break;
            case 2: format_TSCommand( &command, "atanl:$(unifloat)", x);
                break;
            }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}


/** atan2_spec **/
//This mediator refers to: atan2f, atan2, atan2l
mediator atan2_media for 
specification
Unifloat* atan2_spec(CallContext context, Unifloat* y, Unifloat* x,
                     ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "atan2f:$(unifloat)$(unifloat)", y, x);
            break;
        case 1: format_TSCommand( &command, "atan2:$(unifloat)$(unifloat)", y, x);
            break;
        case 2: format_TSCommand( &command, "atan2l:$(unifloat)$(unifloat)", y, x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** cos_spec **/
// This mediator refers to: cosf, cos, cosl
mediator cos_media for 
specification
Unifloat* cos_spec(CallContext context, Unifloat* x,  Unifloat* cosine, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        if ((x->type == UniDoubleT) && (CHECK_EXTENDED_REQ))
            format_TSCommand( &command, "cos_adv:$(unifloat)$(int)", x, create_IntTObj(rounding));        
        else
            switch(x->type) 
            {
            case 0: format_TSCommand( &command, "cosf:$(unifloat)", x);
                break;
            case 1: format_TSCommand( &command, "cos:$(unifloat)", x);
                break;
            case 2: format_TSCommand( &command, "cosl:$(unifloat)", x);
                break;
            }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** sin_spec **/
//This mediator refers to: sinf, sin, sinl
mediator sin_media for 
specification
Unifloat* sin_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "sinf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "sin:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "sinl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}


/** sincos_spec **/
//This mediator refers to: sincosf, sincos, sincosl
mediator sincos_media for 
specification
void sincos_spec(CallContext context, Unifloat* x, Unifloat** sin, Unifloat** cos)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "sincosf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "sincos:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "sincosl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *sin = readUnifloat_TSStream(&command.response);
            *cos = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return;
    }
}

//* tan_spec *
//// This mediator refers to: tanf, tan, tanl
//mediator tan_media for 
//specification
//Unifloat* tan_spec(CallContext context, Unifloat* x, ErrorCode* errno)
//{
//    call
//    {
//        TSCommand command = create_TSCommand();
//        Unifloat* res;
//    
//        switch(x->type) 
//        {
//        case 0: format_TSCommand( &command, "tanf:$(unifloat)", x);
//            break;
//        case 1: format_TSCommand( &command, "tan:$(unifloat)", x);
//            break;
//        case 2: format_TSCommand( &command, "tanl:$(unifloat)", x);
//            break;
//        }
//        
//        executeCommandInContext( context, &command );
//        
//        if (!isBadVerdict())
//        {
//            timestamp = command.meta.timestamp;
//            *errno = readInt_TSStream(&command.response);
//            res = readUnifloat_TSStream(&command.response);
//        }
//
//        destroy_TSCommand(&command);
//        
//        return res;
//    }
//}

mediator tan_media for
specification
Unifloat* tan_spec(CallContext context, Unifloat* x, Unifloat* tangent, IntT rounding, IntT* err_code, IntT* dist,  ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        if ((x->type == UniDoubleT) && (CHECK_EXTENDED_REQ))
            format_TSCommand( &command, "tan_adv:$(unifloat)$(int)", x, create_IntTObj(rounding));        
        else
            switch(x->type) 
            {
                case 0: format_TSCommand( &command, "tanf:$(unifloat)", x);
                    break;
                case 1: format_TSCommand( &command, "tan:$(unifloat)", x);
                    break;
                case 2: format_TSCommand( &command, "tanl:$(unifloat)", x);
                    break;
            }

        executeCommandInContext( context, &command);
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

long double convertUnifloatDouble(Unifloat* x)
{
    long double res = 0;
    int i, n, pos; 
    int exp = 0;
    long double zero = 0.0;

    if (x->kind == NaN) 
        res = zero/zero;
    if (x->kind == Infinity) 
        if (x->sign ==1)
            res = 1/zero;
        else
            res = -1/zero;

    if (x->kind == Normal)
    {
            for (i = digMant_Unifloat(x->type); i > 0; i--)
            {
                exp = x->exp - i;
                res = res + powl(2, exp) * getMant_Unifloat((Unifloat*)x, i);
            }
            res = (x->sign) * res;
    }
    return res;
}

Unifloat* convertDoubleUnifloat(double x)
{
    long *buf = (long*)&x;
    ULongT bit;
    int indexBuf = 0;
    IntT exponent = 0x0;

    int size = (sizeof(double) - 4)/sizeof(long) + 1;
    int i = 0, j = 0x1;

    Unifloat* res = createZero_Unifloat(UniDoubleT);
    while (i < size)
    {
        bit = buf[i] & 0x1;
        buf[i] >>= 1;
        
        if (indexBuf < digMant_DoubleT - 1)
            setMant_Unifloat(res, digMant_DoubleT - indexBuf, bit);
        else if (indexBuf < digMant_DoubleT + digExp_DoubleT)
        {
            exponent += bit*j;
            j <<= 1;
        }
        
        indexBuf++;

        if((indexBuf % (sizeof_LongT * 8) )== 0)
            i++;
    }

    if((exponent == maxExp_DoubleT * 2 - 1)||(exponent == maxExp_DoubleT * 4 - 1))   /* exponent == 0x111...11 */
    {
        j = 0;

        for(i = 0; i < MAX_SIZE_UNIFLOAT; i++) 
            if(res->mant[i] != 0)
                j = 1;

        if(j == 1)
            res->kind = NaN;
        else
            res->kind = Infinity;

        setMant_Unifloat(res, 1, 1);
    }
    else
    {
        res->kind = Normal;
        res->exp = exponent - maxExp_DoubleT + 2;
        if (res->exp > maxExp_DoubleT)
            res->exp -= 2 * maxExp_DoubleT;

        if(exponent == 0x0)
        {
            setMant_Unifloat(res, 1, 0);
            res->exp++;
            res = normalize_Unifloat(res);
            res = round_Unifloat(res, PRECISION);
        }
        else
            setMant_Unifloat(res, 1, 1);
    }

/*    if (exponent == maxExp_DoubleT * 4 - 1)
        res->kind = NaN;*/
    res->sign = 1 - 2 * bit;
    return res;
}

//mediator tan_media for
//specification
//Unifloat* tan_spec(CallContext context, Unifloat* x, Unifloat* tangent, IntT rounding, IntT* err_code, IntT* dist,  ErrorCode* errno)
//{
//    call
//    {
//        long double arg, res1;
//        Unifloat *res = createZero_Unifloat(UniDoubleT);
//        arg = convertUnifloatDouble(x);
//        res1 = tan(arg);
//        
//        res = convertDoubleUnifloat(res1);
//        return res;
//    }
//}
//mediator cos_media for 
//specification
//Unifloat* cos_spec(CallContext context, Unifloat* x,  Unifloat* cosine, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
//{
//    call
//    {
//        long double arg, res1;
//        Unifloat *res = createZero_Unifloat(UniDoubleT);
//        arg = convertUnifloatDouble(x);
//        res1 = cos(arg);
//        
//        res = convertDoubleUnifloat(res1);
//        return res;
//    }
//}

//mediator atan_media for 
//specification
//Unifloat* atan_spec(CallContext context, Unifloat* x, Unifloat* atangent, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
//{
//    call
//    {
//        long double arg, res1;
//        Unifloat *res = createZero_Unifloat(UniDoubleT);
//
//        if (x->exp <= -1022) 
//               return clone(x);
//        arg = convertUnifloatDouble(x);
//        res1 = atan(arg);
//        
//        res = convertDoubleUnifloat(res1);
//        return res;
//    }
//}

//mediator acos_media for 
//specification
//Unifloat* acos_spec(CallContext context, Unifloat* x, Unifloat* acosine, IntT rounding, IntT* err_code, IntT* dist, ErrorCode* errno)
//{
//    call
//    {
//        long double arg, res1;
//        Unifloat *res = createZero_Unifloat(UniDoubleT);
//
//        arg = convertUnifloatDouble(x);
//        res1 = acos(arg);
//        
//        res = convertDoubleUnifloat(res1);
//        return res;
//    }
//}
