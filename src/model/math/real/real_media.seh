/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MATH_REAL_MEDIA_SEH
#define MATH_REAL_MEDIA_SEH

#include "math/real/real_model.seh"

/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initMathRealSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** ceil_spec **/
//This mediator refers to: ceilf, ceil, ceill
mediator ceil_media for 
specification
Unifloat* ceil_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** copysign_spec **/
//This mediator refers to: copysignf, copysingn, copysignl
mediator copysign_media for 
specification
Unifloat* copysign_spec(CallContext context, Unifloat* x, Unifloat* y);

/** drem_spec **/
//This mediator refers to: dremf, drem, dreml
mediator drem_media for 
specification
Unifloat* drem_spec(CallContext context, Unifloat* x, Unifloat* y, ErrorCode* errno);

/** fabs_spec **/
//This mediator refers to: fabsf, fabs, fabsl
mediator fabs_media for
specification
Unifloat* fabs_spec(CallContext context, Unifloat* x);

/** fdim_spec **/
//This mediator refers to: fdimf, fdim, fdiml
mediator fdim_media for 
specification
Unifloat* fdim_spec(CallContext context, Unifloat* x, Unifloat* y,
                    ErrorCode* errno);
/** floor_spec **/
//This mediator refers to: floorf, floor, floorl
mediator floor_media for specification
Unifloat* floor_spec(CallContext context, Unifloat* value, ErrorCode* errno);

/** fma_spec **/
//This mediator refers to: fmaf, fma, fmal
mediator fma_media for specification
Unifloat* fma_spec( CallContext context, Unifloat* x, Unifloat* y, 
                    Unifloat* z, ErrorCode* errno);

/** fmax_spec **/
//This mediator refers to: fmaxf, fmax, fmaxl
mediator fmax_media for specification
Unifloat* fmax_spec( CallContext context, Unifloat* x, Unifloat* y);

/** fmin_spec **/
//This mediator refers to: fminf, fmin, fminl
mediator fmin_media for specification
Unifloat* fmin_spec( CallContext context, Unifloat* x, Unifloat* y);

/** fmod_spec **/
//This mediator refers to: fmodf, fmod, fmodl
mediator fmod_media for specification
Unifloat* fmod_spec( CallContext context, Unifloat* x, 
                     Unifloat* y, ErrorCode *errno);

/** llrint_spec **/
//This mediator refers to: llrintf, llrint, llrintl
mediator llrint_media for 
specification
LLongT llrint_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** llround_spec **/
//This mediator refers to: llroundf, llround, llroundl
mediator llround_media for 
specification
LLongT llround_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** lrint_spec **/
//This mediator refers to: lrintf, lrint, lrintl
mediator lrint_media for 
specification
LongT lrint_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** lround_spec **/
//This mediator refers to: lroundf, lround, lroundl
mediator lround_media for 
specification
LongT lround_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** modf_spec **/
//This mediator refers to: modff, modf, modfl
mediator modf_media for specification
Unifloat* modf_spec( CallContext context, Unifloat* x, Unifloat** iptr);

/** nan_spec **/
//This mediator refers to: nanf, nan, nanl
mediator nan_media for specification
Unifloat* nan_spec( CallContext context, CString* tagp, IntT type_f);

/** nearbyint_spec **/
//This mediator refers to: nearbyintf, nearbyint, nearbyintl
mediator nearbyint_media for
specification
Unifloat* nearbyint_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** remainder_spec **/
//This mediator refers to: remainderf, remainder, remainderl
mediator remainder_media for specification
Unifloat* remainder_spec( CallContext context, Unifloat* x, 
                          Unifloat* y, ErrorCode *errno);

/** remquo_spec **/
//This mediator refers to: remquof, remquo, remquol
mediator remquo_media for specification
Unifloat* remquo_spec( CallContext context, Unifloat* x, 
                       Unifloat* y, Unifloat** quo, ErrorCode *errno);

/** rint_spec **/
//This mediator refers to: rintf, rint, rintl
mediator rint_media for 
specification
Unifloat* rint_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** round_spec **/
//This mediator refers to: roundf, round, roundl
mediator round_media for 
specification
Unifloat* round_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** trunc_spec **/
//This mediator refers to: truncf, trunc, truncl
mediator trunc_media for 
specification
Unifloat* trunc_spec(CallContext context, Unifloat* x);

#endif

