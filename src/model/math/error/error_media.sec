/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "common/common_media.seh"
#include "math/error/error_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initMathErrorSubsystem(void)
{
  // Set up mediators
    set_mediator_erf_spec(erf_media);
    set_mediator_erfc_spec(erfc_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** erf_spec **/
//This mediator refers to: erff, erf, erfl
mediator erf_media for specification
Unifloat* erf_spec( CallContext context, Unifloat* x, ErrorCode* errno )
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
        char st[3][100] = {"erff:$(unifloat)",
                           "erf:$(unifloat)", 
                           "erfl:$(unifloat)"};
    
        format_TSCommand( &command, st[x->type], x);
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}


/** erfc_spec **/
//This mediator refers to: erfcf, erfc, erfcl
mediator erfc_media for specification
Unifloat* erfc_spec( CallContext context, Unifloat* x, ErrorCode* errno )
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
        char st[3][100] = {"erfcf:$(unifloat)",
                           "erfc:$(unifloat)", 
                           "erfcl:$(unifloat)"};
    
        format_TSCommand( &command, st[x->type], x);
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            *errno = readInt_TSStream(&command.response);
            res = readUnifloat_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

