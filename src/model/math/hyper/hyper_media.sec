/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "math/hyper/hyper_media.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initMathHyperSubsystem(void)
{
  // Set up mediators
    set_mediator_acosh_spec(acosh_media);
    set_mediator_asinh_spec(asinh_media);
    set_mediator_atanh_spec(atanh_media);
    set_mediator_cosh_spec(cosh_media);
    set_mediator_sinh_spec(sinh_media);
    set_mediator_tanh_spec(tanh_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** acosh_spec **/
//This mediator refers to: acoshf, acosh, acoshl
mediator acosh_media for 
specification
Unifloat* acosh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "acoshf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "acosh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "acoshl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** asinh_spec **/
//This mediator refers to: asinhf, asinh, asinhl
mediator asinh_media for 
specification
Unifloat* asinh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "asinhf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "asinh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "asinhl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** atanh_spec **/
//This mediator refers to: atanhf, atanh, atanhl
mediator atanh_media for 
specification
Unifloat* atanh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "atanhf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "atanh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "atanhl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}
/** cosh_spec **/
//This mediator refers to: coshf, cosh, coshl
mediator cosh_media for 
specification
Unifloat* cosh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "coshf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "cosh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "coshl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** sinh_spec **/
//This mediator refers to: sinhf, sinh, sinhl
mediator sinh_media for 
specification
Unifloat* sinh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "sinhf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "sinh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "sinhl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);
        
        return res;
    }
}

/** tanh_spec **/
//This mediator refers to: tanhf, tanh, tanhl
mediator tanh_media for 
specification
Unifloat* tanh_spec(CallContext context, Unifloat* x, ErrorCode* errno)
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
    
        switch(x->type) 
        {
        case 0: format_TSCommand( &command, "tanhf:$(unifloat)", x);
            break;
        case 1: format_TSCommand( &command, "tanh:$(unifloat)", x);
            break;
        case 2: format_TSCommand( &command, "tanhl:$(unifloat)", x);
            break;
        }
        
        executeCommandInContext( context, &command );
        
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            *errno = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
}

