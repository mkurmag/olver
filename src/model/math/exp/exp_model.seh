/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MATH_EXP_MODEL_SEH
#define MATH_EXP_MODEL_SEH

#include "common/common_model.seh"
#include "math/math/math_data.seh"

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** cbrt_spec **/
//This specification refers to: cbrtf, cbrt, cbrtl
specification
Unifloat* cbrt_spec(CallContext context, Unifloat* x);
Unifloat* cbrt_model(Unifloat* x);

/** exp_spec **/
//This specification refers to: expf, exp, expl
specification
Unifloat* exp_spec(CallContext context, Unifloat* x, Unifloat* standart, IntT rounding, IntT* dist, ErrorCode* errno);
Unifloat* exp_model(Unifloat* x);

/** exp2_spec **/
//This specification refers to: exp2f, exp2, exp2l
specification
Unifloat* exp2_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* exp2_model(Unifloat* x);

/** expm1_spec **/
//This specification refers to: expm1f, expm1, expm1l
specification
Unifloat* expm1_spec(CallContext context, Unifloat* x, Unifloat* standart, IntT rounding, IntT* dist, ErrorCode* errno);
Unifloat* expm1_model(Unifloat* x);

/** hypot_spec **/
//This specification refers to: hypotf, hypot, hypotl
specification
Unifloat* hypot_spec(CallContext context, Unifloat* x, Unifloat* y, ErrorCode* errno);
Unifloat* hypot_model(Unifloat* x, Unifloat* y);

/** log_spec **/
//This specification refers to: logf, log, logl
specification
Unifloat* log_spec(CallContext context, Unifloat* x, Unifloat* standart, IntT rounding, IntT* dist, ErrorCode* errno);
Unifloat* log_model(Unifloat* x);

/** log10_spec **/
//This specification refers to: log10f, log10, log10l
specification
Unifloat* log10_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* log10_model(Unifloat* x);

/** log1p_spec **/
//This specification refers to: log1pf, log1p, log1pl
specification
Unifloat* log1p_spec(CallContext context, Unifloat* x, Unifloat* standart, IntT rounding, IntT* dist, ErrorCode* errno);
Unifloat* log1p_model(Unifloat* x);

/** log2_spec **/
//This specification refers to: log2f, log2, log2l
specification
Unifloat* log2_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* log2_model(Unifloat* x);

/** pow_spec **/
//This specification refers to: powf, pow, powl
specification
Unifloat* pow_spec(CallContext context, Unifloat* x, Unifloat* y, ErrorCode* errno);
Unifloat* pow_model(Unifloat* x, Unifloat* y);

/** pow10_spec **/
//This specification refers to: pow10f, pow10, pow10l
specification
Unifloat* pow10_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* pow10_model(Unifloat* x);

/** sqrt_spec **/
//This specification refers to: sqrtf, sqrt, sqrtl
specification
Unifloat* sqrt_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* sqrt_model(Unifloat* x);

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

bool isOdd_Unifloat(Unifloat* x);

#endif

