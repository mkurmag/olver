/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MATH_GAMMA_MODEL_SEH
#define MATH_GAMMA_MODEL_SEH

#include "common/common_model.seh"
#include "math/math/math_data.seh"

IntT signgam_model;
/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/
/** gamma_spec **/
//This specification refers to: gammaf, gamma, gammal
specification
Unifloat* gamma_spec(CallContext context, Unifloat* x, 
                      IntT* signgam,  ErrorCode* errno);
Unifloat* gamma_model(Unifloat* x);

/** lgamma_spec **/
//This specification refers to: lgammaf, lgamma, lgammal
specification
Unifloat* lgamma_spec(CallContext context, Unifloat* x, 
                      IntT* signgam,  ErrorCode* errno);
Unifloat* lgamma_model(Unifloat* x);

/** lgamma_r_spec **/
//This specification refers to: lgammaf_r, lgamma_r, lgammal_r
specification
Unifloat* lgamma_r_spec(CallContext context, Unifloat* x, 
                      IntT* signp,  ErrorCode* errno);

/** tgamma_spec **/
//This specification refers to: tgammaf, tgamma, tgammal
specification
Unifloat* tgamma_spec(CallContext context, Unifloat* x, ErrorCode* errno);
Unifloat* tgamma_model(Unifloat* x);

/********************************************************************/
/**                     Unifloat gamma functions                   **/
/********************************************************************/

Unifloat* gammaSeries_Unifloat(Unifloat* x);

#endif

