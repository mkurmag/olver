/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "ncurses/bkgd/bkgd_media.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesBkgdSubsystem(void)
{
    // Set up mediators
    set_mediator_bkgd_spec(bkgd_media);
    set_mediator_bkgdset_spec(bkgdset_media);
    set_mediator_getbkgd_spec(getbkgd_media);

    // Set up formatters
    registerTSFormatter("chtype",(TSFormatterFuncType)chtype_formatter);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** bkgd_spec **/
//This mediator refers to: bkgd, wbkgd
mediator bkgd_media for specification
IntT bkgd_spec(CallContext context, WindowTPtr* win, ChTypeT ch)
{
    call
    {
        IntT res;
        TSCommand command = create_TSCommand();

        if (win == NULL)
        {
            format_TSCommand(&command, "bkgd:$(chtype)",
                             create_ChTypeTObj(ch)
                            );
        }
        else
        {
            format_TSCommand(&command, "wbkgd:$(ptr)$(chtype)",
                             create_VoidTPtrObj(*win),
                             create_ChTypeTObj(ch)
                            );
        }

        executeCommandInContext(context, &command);
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);

        return res;
    }
    state
    {
        onNCursesUniversal(context, win, bkgd_spec);
    }
}


/** bkgdset_spec **/
//This mediator refers to: bkgdset, wbkgdset
mediator bkgdset_media for specification
void bkgdset_spec(CallContext context, WindowTPtr* win, ChTypeT ch)
{
    call
    {
        TSCommand command = create_TSCommand();

        if (win == NULL)
        {
            format_TSCommand(&command, "bkgdset:$(chtype)",
                             create_ChTypeTObj(ch)
                            );
        }
        else
        {
            format_TSCommand(&command, "wbkgdset:$(ptr)$(chtype)",
                             create_VoidTPtrObj(*win),
                             create_ChTypeTObj(ch)
                            );
        }

        executeCommandInContext(context, &command);
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
        }

        destroy_TSCommand(&command);
    }
    state
    {
        onNCursesUniversal(context, win, SUT_OK);
    }
}


/** getbkgd_spec **/
mediator getbkgd_media for specification
ChTypeT getbkgd_spec(CallContext context, WindowTPtr* win)
{
    call
    {
        ChTypeT res;
        TSCommand command = create_TSCommand();

        format_TSCommand(&command, "getbkgd:$(ptr)",
            create_VoidTPtrObj(*win)
                        );
        executeCommandInContext(context, &command);
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readChTypeT_TSStream(&command.response);
        }

        destroy_TSCommand(&command);

        return res;
    }
}


