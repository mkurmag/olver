/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */




#include "ncurses/ncurses/ncurses_data.seh"
#include "ncurses/chgat/chgat_media.seh"
#include "common/common_media.seh"



/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesChgatSubsystem(void)
{
  // Set up mediators
    set_mediator_chgat_spec(chgat_media);

    // Set up formatters
    registerTSFormatter("curses_attr",(TSFormatterFuncType)curses_attr_formatter);
    

}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** chgat_spec **/
//This mediator refers to: mvwchgat, wchgat, mvchgat, chgat
mediator chgat_media for specification
IntT chgat_spec(CallContext context, WindowTPtr* win, NCursesPosition* pos, 
                IntT n, NcursesAttrT attr, IntT color)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res=-1;
        
        if (win!=NULL)
        {
            if (pos!=NULL)
            {
                format_TSCommand( &command, "mvwchgat:$(ptr)$(int)$(int)$(int)$(curses_attr)$(int)", create_VoidTPtrObj(*win), create_IntTObj(pos->y), create_IntTObj(pos->x), create_IntTObj(n), create_NcursesAttrTObj(attr), create_IntTObj(color));
            }
            else
            {
                format_TSCommand( &command, "wchgat:$(ptr)$(int)$(curses_attr)$(int)", create_VoidTPtrObj(*win), create_IntTObj(n), create_NcursesAttrTObj(attr), create_IntTObj(color));
            }
        }
        else
        {
            if (pos!=NULL)
            {
                format_TSCommand( &command, "mvchgat:$(int)$(int)$(int)$(curses_attr)$(int)", create_IntTObj(pos->y), create_IntTObj(pos->x), create_IntTObj(n), create_NcursesAttrTObj(attr), create_IntTObj(color));
            }
            else
            {
                format_TSCommand( &command, "chgat:$(int)$(curses_attr)$(int)", create_IntTObj(n), create_NcursesAttrTObj(attr), create_IntTObj(color));
            }
        }
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res=readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res; 
    }
    state
    {
        onChgat(context, win, pos, n, attr,  color, chgat_spec);
    }
}


