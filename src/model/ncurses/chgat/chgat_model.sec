/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/*
 * Portions of this text are reprinted and reproduced in electronic form
 * from the Single UNIX Specification Version 2, Copyright (C) 1997 by The Open
 * Group. In the event of any discrepancy between this version and the original
 * document from the Open Group, the Open Group document is the referee document.
 * The original document can be obtained online at http://www.unix.org/version2/online.html.
 */

#include "ncurses/ncurses/ncurses_data.seh"
#include "data/ncurses_model.seh"
#include "ncurses/chgat/chgat_model.seh"


#pragma SEC subsystem chgat "ncurses.chgat"



/*
   The group of functions 'ncurses.chgat' consists of:
       chgat [1]
       mvchgat [1]
       mvwchgat [1]
       wchgat [1]
 */

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/*
Linux Standard Base Core Specification 3.1
Copyright (c) 2004, 2005 Free Standards Group

  refers

The Single UNIX � Specification, Version 2
Copyright (c) 1997 The Open Group

-------------------------------------------------------------------------------

NAME

    chgat, mvchgat, mvwchgat, wchgat - change renditions of characters in a window

SYNOPSIS

    #include <curses.h>
    int chgat(int n, attr_t attr, short color, const void *opts);
    int mvchgat(int y, int x, int n, attr_t attr, short color,
        const void *opts);
    int mvwchgat(WINDOW *win, int y, int x, int n, attr_t attr,
        short color, const void *opts);
    int wchgat(WINDOW *win, int n, attr_t attr, short color,
        const void *opts);


DESCRIPTION

    These functions change the renditions of the next n characters in the
    current or specified window (or of the remaining characters on the current
    or specified line, if n is -1), starting at the current or specified cursor
    position. The attributes and colors are specified by attr and color as for
    setcchar().

    These functions do not update the cursor.These functions do not perform
    wrapping.

    A value of n that is greater than the remaining characters on a line is not
    an error.

    The opts argument is reserved for definition in a future edition of this
    document. Currently, the application must provide a null pointer as opts.

RETURN VALUE

    Upon successful completion, these functions return OK. Otherwise, they
    return ERR.

ERRORS

    No errors are defined.
*/

//This specification refers to: mvwchgat, wchgat, mvchgat, chgat
specification
IntT chgat_spec(CallContext context, WindowTPtr* win, NCursesPosition* pos,
                IntT n, NcursesAttrT attr, IntT color)
{
    NCursesWindow* oldWindow=clone(getWindowMayNULL(context, win));
    NCursesWindow* modifiedWindow;

    if (win!=NULL){
        if (pos!=NULL){
            FILTER("mvwchgat");
        }
        else{
            FILTER("wchgat");
        }
    }
    else{
        if (pos!=NULL){
            FILTER("mvchgat");
        }
        else{
            FILTER("chgat");
        }
    }

    pre
    {
        modifiedWindow = chgat_model(context, win, pos, n, attr, color);

        return true;
    }
    post
    {
        if (chgat_spec!=SUT_EOK)
        {
            /*
            * Otherwise, they return ERR.
            */
            REQ("chgat.07.02;mvchgat.07.02;mvwchgat.07.02;wchgat.07.02",
                "Function shall return ERR", chgat_spec==SUT_ERR);
        }
        else
        {
            NCursesWindow* curWnd=getWindowMayNULL(context, win);

            /*
            * Upon successful completion, these functions return OK.
            */
            REQ("chgat.07.01", "Function shall return OK", chgat_spec==SUT_EOK);


            if (pos!=NULL)
            {
                /*
                * These functions do not update the cursor.
                */
                REQ("chgat.03;mvchgat.03;mvwchgat.03;wchgat.03", "Cursor position shall be valid",
                    equals(curWnd->cursorPos, pos));
            }
            else
            {
                /*
                * These functions do not update the cursor.
                */
                REQ("chgat.03;mvchgat.03;mvwchgat.03;wchgat.03", "Cursor position shall be valid",
                    equals(curWnd->cursorPos, oldWindow->cursorPos));
            }

            /*
            * [Compare model window with the real window]
            */
            REQ("", "Data in the window shall be valid", check_NCursesWindows(curWnd, modifiedWindow, CH_ALL, 10));
        }

        return true;
    }
    FILTER_CLEAN;
}


NCursesWindow* chgat_model(CallContext context, WindowTPtr* win, NCursesPosition* pos,
                IntT n, NcursesAttrT attr, IntT color)
{
    NCursesWindow*   curWnd;
    NCursesPosition* curPos;
    IntT             insPos, i;


    curWnd=createNCursesWindow_Local(getWindowMayNULL(context, win));
    curPos=getWindowPositionMayNULL(curWnd, pos);

    /*
    * starting at the current or specified cursor position.
    */
    IMPLEMENT_REQ("chgat.01.03;mvchgat.01.03;mvwchgat.01.03;wchgat.01.03");

    insPos = curPos->y*curWnd->ncols+curPos->x;

    if (n==-1)
    {
        /*
        * or of the remaining characters on the current or specified line, if n is -1
        */
        IMPLEMENT_REQ("chgat.01.02;mvchgat.01.02;mvwchgat.01.02;wchgat.01.02");

        n = curWnd->ncols - curPos->x;
    }

    if (n > curWnd->ncols - curPos->x)
    {
        /*
        * A value of n that is greater than the remaining characters on a line is not an
        * error.
        */
        IMPLEMENT_REQ("chgat.05;mvchgat.05;mvwchgat.05;wchgat.05");

        n = curWnd->ncols - curPos->x;
    }

    /*
    * These functions change the renditions of the next n characters in the current
    * or specified window
    */
    IMPLEMENT_REQ("chgat.01.01;mvchgat.01.01;mvwchgat.01.01;wchgat.01.01");

    /*
    * These functions do not perform wrapping.
    */
    IMPLEMENT_REQ("chgat.04;mvchgat.04;mvwchgat.04;wchgat.04");

    for (i=0;i<n;i++)
    {
        ChTypeT curch = getElement_NCursesWindow_Offset(curWnd, insPos+i);

        /*
        * The attributes and colors are specified by attr and color as for setcchar().
        */
        IMPLEMENT_REQ("chgat.02;mvchgat.02;mvwchgat.02;wchgat.02");

        curch.altcharset = attr.altcharset;
        curch.blink = attr.blink;
        curch.bold = attr.bold;
        curch.dim = attr.dim;
        curch.invis = attr.invis;
        curch.protect = attr.protect;
        curch.reverse = attr.reverse;
        curch.standout = attr.standout;
        curch.underline = attr.underline;

        curch.colorPair = color;

        setElement_NCursesWindow_Offset(curWnd, insPos+i, curch);
    }

    curWnd->cursorPos->x = curPos->x;
    curWnd->cursorPos->y = curPos->y;

    pushNCursesWindow(curWnd);

    return curWnd;
}


void onChgat(CallContext context, WindowTPtr* win, NCursesPosition* pos,
                IntT n, NcursesAttrT attr, IntT color, IntT chgat_spec)
{
    if (chgat_spec==SUT_EOK)
    {
        updateNCursesWindowMayNULL(context, win);
        refresh_WindowTPtr(context, win, 0);
    }
}


/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/
