/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef NCURSES_TERMINAL_TERMINFO_MEDIA_SEH
#define NCURSES_TERMINAL_TERMINFO_MEDIA_SEH

#include "ncurses/terminal/terminfo_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesTerminalTerminfoSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** del_curterm_spec **/
mediator del_curterm_media for specification
IntT del_curterm_spec(CallContext context, TerminalTPtr oterm);

/** putp_spec **/
//This mediator refers to: putp, tputs
mediator putp_media for specification
IntT putp_spec(CallContext context, CString* str, IntT affcnt, bool putfunc, IntT standardKey);

/** set_curterm_spec **/
mediator set_curterm_media for specification
SetCurTermReturnType* set_curterm_spec(CallContext context, TerminalTPtr new_term);

/** setupterm_spec **/
//This mediator refers to: restartterm, setupterm
mediator setupterm_media for specification
SetupTermReturnType* setupterm_spec(CallContext context, CString* term, bool restart);

/** termname_spec **/
mediator termname_media for specification
CString* termname_spec(CallContext context);

/** tigetflag_spec **/
mediator tigetflag_media for specification
IntT tigetflag_spec(CallContext context, CString* capname);

/** tigetnum_spec **/
mediator tigetnum_media for specification
IntT tigetnum_spec(CallContext context, CString* capname);

/** tigetstr_spec **/
mediator tigetstr_media for specification
StringTPtr tigetstr_spec(CallContext context, CString* capname);

/** tparm_spec **/
mediator tparm_media for specification
StringTPtr tparm_spec(CallContext context, CString* cap, LongT p1, LongT p2, LongT p3,
                      LongT p4, LongT p5, LongT p6, LongT p7, LongT p8, LongT p9);

#endif
