/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef NCURSES_INPUT_MODEL_SEH
#define NCURSES_INPUT_MODEL_SEH

#include "common/common_model.seh"
#include "ncurses/ncurses/ncurses_data.seh"

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** cbreak_spec **/
specification
IntT cbreak_spec( CallContext context);
void onCbreak( CallContext context, IntT cbreak_spec);

/** flushinp_spec **/
specification
IntT flushinp_spec( CallContext context);
void onFlushinp( CallContext context, IntT flushinp_spec);

/** halfdelay_spec **/
specification
IntT halfdelay_spec( CallContext context, IntT tenth );
void onHalfdelay( CallContext context, IntT tenth, IntT halfdelay_spec );

/** nl_spec **/
specification
IntT nl_spec( CallContext context);
void onNl( CallContext context, IntT nl_spec);

/** nocbreak_spec **/
specification
IntT nocbreak_spec( CallContext context);
void onNocbreak( CallContext context, IntT nocbreak_spec);

/** nonl_spec **/
specification
IntT nonl_spec( CallContext context);
void onNonl( CallContext context, IntT nonl_spec);

/** noraw_spec **/
specification
IntT noraw_spec( CallContext context);
void onNoraw( CallContext context, IntT noraw_spec);

/** raw_spec **/
specification
IntT raw_spec( CallContext context);
void onRaw( CallContext context, IntT raw_spec);

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

#endif

