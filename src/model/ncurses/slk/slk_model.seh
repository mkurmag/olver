/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef NCURSES_SLK_MODEL_SEH
#define NCURSES_SLK_MODEL_SEH

#include "common/common_model.seh"
#include "ncurses/ncurses/ncurses_data.seh"

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** slk_attrset_spec **/
specification
IntT slk_attrset_spec( CallContext context, ChTypeT attrs );

void onSlkAttrset(CallContext callcontext, ChTypeT attrs, IntT slk_attrset_spec );

/** slk_attr_set_spec **/
specification
IntT slk_attr_set_spec( CallContext context, NcursesAttrT attrs, ShortT color_pair_number, VoidTPtr opt );

void onSlkAttrSet(CallContext context, NcursesAttrT attrs, ShortT color_pair_number, VoidTPtr opt, IntT slk_attr_set_spec );

/** slk_attroff_spec **/
specification
IntT slk_attroff_spec( CallContext context, ChTypeT attrs);

void onSlkAttroff(CallContext context, ChTypeT attrs, IntT slk_attroff_spec );

/** slk_attron_spec **/
specification
IntT slk_attron_spec( CallContext context, ChTypeT attrs);

void onSlkAttron(CallContext context, ChTypeT attrs, IntT slk_attron_spec );

/** slk_clear_spec **/
specification
IntT slk_clear_spec( CallContext context);

void onSlkClear(CallContext context, IntT slk_clear_spec);

/** slk_color_spec **/
specification
IntT slk_color_spec( CallContext context, ShortT color_pair_number);

void onSlkColor(CallContext context, ShortT color_pair_number, IntT slk_color_spec );

/** slk_init_spec **/
specification
IntT slk_init_spec( CallContext context, IntT fmt);

void onSlkInit(CallContext context, IntT fmt, IntT slk_init_spec);

/** slk_label_spec **/
specification
CString * slk_label_spec( CallContext context, IntT labnum );

/** slk_refresh_spec **/
specification
IntT slk_refresh_spec( CallContext context );

void onSlkRefresh(CallContext context, IntT slk_refresh_spec);

/** slk_restore_spec **/
specification
IntT slk_restore_spec( CallContext context);

void onSlkRestore(CallContext context, IntT slk_restore_spec);

/** slk_set_spec **/
specification
IntT slk_set_spec( CallContext context, IntT labnum, CString *label, IntT justify);

void onSlkSet(CallContext context, IntT labnum, CString *label, IntT justify, IntT slk_set_spec);

/** slk_noutrefresh_spec **/
specification
IntT slk_noutrefresh_spec( CallContext context);

void onSlkNoutrefresh(CallContext context, IntT slk_noutrefresh_spec);

/** slk_touch_spec **/
specification
IntT slk_touch_spec( CallContext context );

void onSlkTouch(CallContext context, IntT slk_touch_spec);

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

IntT my_slkIsSoftKey(CallContext context, IntT x, NcursesAttrT attrs);
IntT my_slkReadSoftKey(CallContext context, IntT * x, IntT *res_lspace, IntT * res_rspace, CString ** label, NcursesAttrT attrs);
IntT my_slkSkipSpace(CallContext context, IntT * x, NcursesAttrT attrs);

#endif

