/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "ncurses/clear/clear_media.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesClearSubsystem(void)
{
  // Set up mediators
    set_mediator_clear_spec(clear_media);
    set_mediator_clrtobot_spec(clrtobot_media);
    set_mediator_clrtoeol_spec(clrtoeol_media);
    set_mediator_delch_spec(delch_media);
    set_mediator_deleteln_spec(deleteln_media);
    set_mediator_erasechar_spec(erasechar_media);
//    set_mediator_mvdelch_spec(mvdelch_media);
//    set_mediator_mvwdelch_spec(mvwdelch_media);
//    set_mediator_wclear_spec(wclear_media);
//    set_mediator_wclrtobot_spec(wclrtobot_media);
//    set_mediator_wclrtoeol_spec(wclrtoeol_media);
//    set_mediator_wdelch_spec(wdelch_media);
//    set_mediator_wdeleteln_spec(wdeleteln_media);
//    set_mediator_werase_spec(werase_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** clear_spec **/
//This mediator refers to: werase, wclear, erase, clear
mediator clear_media for specification
IntT clear_spec( CallContext context, WindowTPtr* win, bool erase)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
    
        if(win!=NULL)
        {
            if(erase)
            {            
                format_TSCommand( &command, "werase:$(ptr)",
                    create_VoidTPtrObj(*win));
            }
            else
            {            
                format_TSCommand( &command, "wclear:$(ptr)",
                    create_VoidTPtrObj(*win));
            }
        }
        else
        {
            if(erase)
                format_TSCommand( &command, "erase");
            else
                format_TSCommand( &command, "clear");
        }

        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        
        //onClear(context, win, erase, clear_spec);
        onNCursesUniversal( context, win, clear_spec);
    }
}



/** clrtobot_spec **/
//This mediator refers to: wclrtobot, clrtobot
mediator clrtobot_media for specification
IntT clrtobot_spec( CallContext context, WindowTPtr* win)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
    
        if(win != NULL)
        {
            format_TSCommand( &command, "wclrtobot:$(ptr)",
                create_VoidTPtrObj(*win));
        }
        else
        {
            format_TSCommand( &command, "clrtobot" );
        }

        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        //onClrtobot(context, win, clrtobot_spec);
        onNCursesUniversal( context, win, clrtobot_spec);
    }
}



/** clrtoeol_spec **/
//This mediator refers to: wclrtoeol, clrtoeol
mediator clrtoeol_media for specification
IntT clrtoeol_spec( CallContext context, WindowTPtr* win)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
    
        if(win != NULL)
        {
            format_TSCommand( &command, "wclrtoeol:$(ptr)",
                create_VoidTPtrObj(*win));
        }
        else
        {
            format_TSCommand( &command, "clrtoeol" );
        }

        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        //onClrtoeol(context, win, clrtoeol_spec);
        onNCursesUniversal( context, win, clrtoeol_spec);
    }
}


/** delch_spec **/
//This mediator refers to: mvwdelch, wdelch, mvdelch, delch
mediator delch_media for specification
IntT delch_spec( CallContext context, WindowTPtr* win,
                NCursesPosition* position)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
    
        if(win != NULL)
        {
            if(position != NULL)
            {
                format_TSCommand( &command, "mvwdelch:$(ptr)$(int)$(int)",
                    create_VoidTPtrObj(*win), create_IntTObj(position->y),
                    create_IntTObj(position->y));
            }
            else
            {
                format_TSCommand( &command, "wdelch:$(ptr)",
                    create_VoidTPtrObj(*win));
            }
        }
        else
        {
            if(position != NULL)
            {
                format_TSCommand( &command, "mvdelch:$(int)$(int)",
                    create_IntTObj(position->y), create_IntTObj(position->y));
            }
            else
            {
                format_TSCommand( &command, "delch:");
            }
        }
        
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        //onDelch(context, win, position, delch_spec);
        onNCursesUniversal( context, win, delch_spec);
    }
}


/** deleteln_spec **/
//This mediator refers to: wdeleteln, deleteln
mediator deleteln_media for specification
IntT deleteln_spec( CallContext context, WindowTPtr* win)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
    
        if(win != NULL)
        {        
            format_TSCommand( &command, "wdeleteln:$(ptr)",
                create_VoidTPtrObj(*win));
        }
        else
        {
            format_TSCommand( &command, "deleteln" );
        }

        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        //onDeleteln(context, win, deleteln_spec);
        onNCursesUniversal( context, win, deleteln_spec);
    }
}



/** erasechar_spec **/
//This mediator refers to: killchar, erasechar
mediator erasechar_media for specification
IntT erasechar_spec( CallContext context, bool kill)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res;
            
        if(kill)
        {
            format_TSCommand( &command, "killchar" );
        }
        else
        {
            format_TSCommand( &command, "erasechar" );
        }

        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readInt_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        return res;
    }
    state
    {
        onErasechar(context, kill, erasechar_spec);
    }
}


