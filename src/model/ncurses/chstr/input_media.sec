/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "ncurses/ncurses/ncurses_data.seh"
#include "ncurses/chstr/input_media.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Readers                                     **/
/********************************************************************/

ChTypeTArray* readChTypeTArray_TSStream(TSStream* stream)
{
    ChTypeTArray* res;
    IntT size = readInt_TSStream(stream);
    IntT i;

    res=create_ChTypeTArray(size);
    for (i=0;i<size;i++)
    {
        res->arr[i]= readChTypeT_TSStream(stream);
    }

    return res;
}


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesChstrInputSubsystem(void)
{
  // Set up mediators
    set_mediator_inchstr_spec(inchstr_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** inchstr_spec **/
//This mediator refers to: mvwinchnstr, winchnstr, mvwinchstr, winchstr, 
//mvinchnstr, inchnstr, mvinchstr, inchstr
mediator inchstr_media for specification
InchnStrReturnType* inchstr_spec( CallContext context, IntT* n, WindowTPtr* win, NCursesPosition* pos)
{
    call
    {
        TSCommand command = create_TSCommand();
        IntT res=-1;
        ChTypeTArray* chstr;
        InchnStrReturnType* rval;

        if (win!=NULL)
        {
            if (n!=NULL)
            {
                if (pos!=NULL)
                {
                    format_TSCommand( &command, "mvwinchnstr:$(ptr)$(int)$(int)$(int)", create_VoidTPtrObj(*win), create_IntTObj(pos->y), create_IntTObj(pos->x), create_IntTObj(*n));
                }
                else
                {
                    format_TSCommand( &command, "winchnstr:$(ptr)$(int)", create_VoidTPtrObj(*win), create_IntTObj(*n));
                }
            }
            else
            {
                if (pos!=NULL)
                {
                    format_TSCommand( &command, "mvwinchstr:$(ptr)$(int)$(int)", create_VoidTPtrObj(*win), create_IntTObj(pos->y), create_IntTObj(pos->x));
                }
                else
                {
                    format_TSCommand( &command, "winchstr:$(ptr)", create_VoidTPtrObj(*win));
                }
            }
        }
        else
        {
            if (n!=NULL)
            {
                if (pos!=NULL)
                {
                    format_TSCommand( &command, "mvinchnstr:$(int)$(int)$(int)", create_IntTObj(pos->y), create_IntTObj(pos->x), create_IntTObj(*n));
                }
                else
                {
                    format_TSCommand( &command, "inchnstr:$(int)", create_IntTObj(*n));
                }
            }
            else
            {
                if (pos!=NULL)
                {
                    format_TSCommand( &command, "mvinchstr:$(int)$(int)", create_IntTObj(pos->y), create_IntTObj(pos->x));
                }
                else
                {
                    format_TSCommand( &command, "inchstr");
                }
            }
        }
        executeCommandInContext( context, &command );
        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
         
            res=readInt_TSStream(&command.response);
            chstr=readChTypeTArray_TSStream(&command.response);
        }
        
        destroy_TSCommand(&command);
        
        rval = create_InchnStrReturnType( res, chstr); 
        rval->chstr=chstr;
        return rval;
    }
    state
    {
        onInchstr(context, n, win, pos, inchstr_spec);
    }
}


