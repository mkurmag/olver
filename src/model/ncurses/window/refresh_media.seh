/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef NCURSES_WINDOW_REFRESH_MEDIA_SEH
#define NCURSES_WINDOW_REFRESH_MEDIA_SEH

#include "ncurses/window/refresh_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initNcursesWindowRefreshSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** doupdate_spec **/
mediator doupdate_media for specification
IntT doupdate_spec( CallContext context);

/** is_linetouched_spec **/
mediator is_linetouched_media for specification
IntT is_linetouched_spec( CallContext context, NCursesWindow* win,  IntT line);

/** is_wintouched_spec **/
mediator is_wintouched_media for specification
IntT is_wintouched_spec( CallContext context, NCursesWindow* win);

/** isendwin_spec **/
mediator isendwin_media for specification
IntT isendwin_spec( CallContext context);

/** redrawwin_spec **/
mediator redrawwin_media for specification
IntT redrawwin_spec( CallContext context, NCursesWindow* win);

/** refresh_spec **/
//This mediator refers to: refresh, wrefresh
mediator refresh_media for specification
IntT refresh_spec( CallContext context, WindowTPtr * win );
bool refreshCall( CallContext context, WindowTPtr * win );

/** syncok_spec **/
mediator syncok_media for specification
IntT syncok_spec( CallContext context, NCursesWindow* win, IntT bf);

/** touchline_spec **/
mediator touchline_media for specification
IntT touchline_spec( CallContext context, NCursesWindow* win,  IntT start,
                    IntT count);

/** touchwin_spec **/
mediator touchwin_media for specification
IntT touchwin_spec( CallContext context, NCursesWindow* win);

/** untouchwin_spec **/
mediator untouchwin_media for specification
IntT untouchwin_spec( CallContext context, NCursesWindow* win);

/** wcursyncup_spec **/
mediator wcursyncup_media for specification
void wcursyncup_spec( CallContext context, NCursesWindow* win );

/** wnoutrefresh_spec **/
mediator wnoutrefresh_media for specification
IntT wnoutrefresh_spec( CallContext context, NCursesWindow*  win);

/** wredrawln_spec **/
mediator wredrawln_media for specification
IntT wredrawln_spec( CallContext context, NCursesWindow* win,  IntT beg_line,
                    IntT num_lines);

/** wrefresh_spec **/
//mediator wrefresh_media for specification
//ReturnType wrefresh_spec( CallContext context, ... );

/** wsyncdown_spec **/
mediator wsyncdown_media for specification
void wsyncdown_spec( CallContext context, NCursesWindow* win);

/** wsyncup_spec **/
mediator wsyncup_media for specification
void wsyncup_spec( CallContext context, NCursesWindow* win);

/** wtouchln_spec **/
mediator wtouchln_media for specification
IntT wtouchln_spec( CallContext context, NCursesWindow* win, IntT y, IntT n,
                   IntT changed);

#endif

