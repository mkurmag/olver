/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NCURSES_STRING_PRINTF_MEDIA_SEH
#define NCURSES_STRING_PRINTF_MEDIA_SEH

#include "ncurses/string/printf_model.seh"

/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/

void initNcursesStringPrintfSubsystem(void);

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** mvprintw_spec **/
//mediator mvprintw_media for specification
//ReturnType mvprintw_spec( CallContext context, ... );

/** mvwprintw_spec **/
//mediator mvwprintw_media for specification
//ReturnType mvwprintw_spec( CallContext context, ... );

/** printw_spec **/
mediator printw_media for specification
IntT printw_spec
         ( CallContext context, WindowTPtr * win, NCursesPosition * pos, CString * fmt, List /* NULL */ * arguments );

/** vw_printw_spec **/
mediator vw_printw_media for specification
IntT vw_printw_spec( CallContext context, WindowTPtr * win, CString * fmt, List /* NULL */ * arguments );

/** vwprintw_spec **/
mediator vwprintw_media for specification
IntT vwprintw_spec( CallContext context, WindowTPtr * win, CString * fmt, List /* NULL */ * arguments );

/** wprintw_spec **/
//mediator wprintw_media for specification
//ReturnType wprintw_spec( CallContext context, ... );

#endif
