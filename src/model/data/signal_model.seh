/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DATA_SIGNAL_MODEL_SEH
#define DATA_SIGNAL_MODEL_SEH

#include "data/signal_config.h"
#include "data/embed_model.seh"
#include "atl/set.h"
#include "system/sysconf/sysconf_model.seh"
#include "signal/sigset/sigset_model.seh"

/* 
 * This file represents data definition requirements of
 *   Linux Standard Base Core Specification 3.1
 *   Copyright (c) 2004, 2005 Free Standards Group
 *
 *   See section 'Data Definitions for libc', header file 'signal.h'.
 */


/********************************************************************/
/**                              Signal                            **/
/********************************************************************/
typedef IntTObj Signal;
typedef IntT    SignalVal;


#define SUT_SIGHUP      SignalHUP
#define SUT_SIGINT      SignalINT
#define SUT_SIGQUIT     SignalQUIT
#define SUT_SIGILL      SignalILL
#define SUT_SIGTRAP     SignalTRAP
#define SUT_SIGABRT     SignalABRT
#define SUT_SIGIOT      SignalIOT
#define SUT_SIGBUS      SignalBUS
#define SUT_SIGFPE      SignalFPE
#define SUT_SIGKILL     SignalKILL
#define SUT_SIGUSR1     SignalUSR1
#define SUT_SIGSEGV     SignalSEGV
#define SUT_SIGUSR2     SignalUSR2
#define SUT_SIGPIPE     SignalPIPE
#define SUT_SIGALRM     SignalALRM
#define SUT_SIGTERM     SignalTERM
#define SUT_SIGSTKFLT   SignalSTKFLT
#define SUT_SIGCHLD     SignalCHLD
#define SUT_SIGCONT     SignalCONT
#define SUT_SIGSTOP     SignalSTOP
#define SUT_SIGTSTP     SignalTSTP
#define SUT_SIGTTIN     SignalTTIN
#define SUT_SIGTTOU     SignalTTOU
#define SUT_SIGURG      SignalURG
#define SUT_SIGXCPU     SignalXCPU
#define SUT_SIGXFSZ     SignalXFSZ
#define SUT_SIGVTALRM   SignalVTALRM
#define SUT_SIGPROF     SignalPROF
#define SUT_SIGWINCH    SignalWINCH
#define SUT_SIGIO       SignalIO
#define SUT_SIGPWR      SignalPWR
#define SUT_SIGSYS      SignalSYS
#define SUT_SIGUNUSED   SignalUNUSED
#define SUT_SIGCLD      SUT_SIGCHLD
#define SUT_SIGPOLL     SUT_SIGIO
#define SUT_WRONG_SIGNAL_NUMBER -1


/*
 * Number of supported usual signals. It is implied, that signals are in 
 * interval [1, SIGRTDEF]
 */
#define SIGDEFF 31

/*
 * Option, if realtime signals are processed in this system  
 */
#define REALTIME_OPTION true//POSIX_OPTION(context, RTS)

typedef IntTObj SignalObj;
#define type_SignalObj   type_IntTObj
#define create_SignalObj create_IntTObj

typedef IntTObj RTSignalValueObj;
#define type_RTSignalValueObj   type_IntTObj
#define create_RTSignalValueObj create_IntTObj


/********************************************************************/
/**                          SigMaskAction                         **/
/********************************************************************/
typedef IntT SigMaskAction;

#define SUT_SIG_BLOCK     0
#define SUT_SIG_UNBLOCK   1
#define SUT_SIG_SETMASK   2

/********************************************************************/
/**                          Signal_mask                           **/
/********************************************************************/
#define CALLED_NONE                 -1
#define CALLED_SIGPROCMASK          1
#define CALLED_PTHREAD_SIGMASK      2
#define CALLED_HANDLER              3

specification typedef struct CalledFunctionDescription
{
    /*
     * Unique identificator of function call, used to remember curent function
     */
    int uid;
    int function_called;
    int function_nested;
} CalledFunctionDescription;

/********************************************************************/
/*                         Signal Action Flags                      */
/********************************************************************/
#define SUT_SA_NOCLDSTOP  1
#define SUT_SA_ONSTACK    2
#define SUT_SA_RESETHAND  4
#define SUT_SA_RESTART    8
#define SUT_SA_INTERRUPT  16
#define SUT_SA_SIGINFO    32
#define SUT_SA_NOCLDWAIT  64
#define SUT_SA_NODEFER    128


/********************************************************************/
/**                         Signal Handler                         **/
/********************************************************************/
typedef enum SignalHandler
{
  SUT_SIG_DFL,
  SUT_SIG_ERR,
  SUT_SIG_HOLD,
  SUT_SIG_IGN,
  SIG_USER_CATCHER,
  SIG_USER_AGENT,
  SIG_UNDEFINED
} SignalHandler;

typedef IntTObj SignalHandlerObj;
#define type_SignalHandlerObj   type_IntTObj
#define create_SignalHandlerObj create_IntTObj

typedef enum SignalSigactionHandler
{
  SUT_SIGACT_DFL,
  SUT_SIGACT_ERR,
  SUT_SIGACT_HOLD,
  SUT_SIGACT_IGN,
  SIGACT_USER_CATCHER,
  SIGACT_USER_AGENT,
  SIGACT_UNDEFINED
} SignalSigactionHandler;

typedef IntTObj SignalSigactionHandlerObj;
#define type_SignalSigactionHandlerObj   type_IntTObj
#define create_SignalSigactionHandlerObj create_IntTObj


/********************************************************************/
/**                     SigAction Functions                        **/
/********************************************************************/

typedef VoidTPtr SigActionTPtr;

typedef struct SigActionFlag
{
    bool SA_NOCLDSTOP_Flag;
    bool SA_ONSTACK_Flag;
    bool SA_RESETHAND_Flag;
    bool SA_RESTART_Flag;
    bool SA_INTERRUPT_Flag;
    bool SA_SIGINFO_Flag;
    bool SA_NOCLDWAIT_Flag;
    bool SA_NODEFER_Flag;
} SigActionFlag;

specification typedef struct SigAction
{
    SignalHandler m_sa_handler;
    Set* m_sa_mask;
    SigActionFlag m_sa_flag;
    SignalSigactionHandler m_sa_sigaction;    
} SigAction;

specification typedef struct SignalValue
{
    bool is_int;
    IntT int_value;
    VoidTPtr ptr_value;
} SignalValue;

specification typedef struct SignalParams
{
    SignalValue*     sig_val;
    CallContext call_context;
    IntT           unique_id;
    bool       returned_kill; //true if success kill reaction get before signal handler
} SignalParams;

typedef VoidTPtr SigInfoTPtr;

specification typedef struct SigInfo
{
    VoidTPtr     address;
    IntT         m_si_signo;
    IntT         m_si_code;
    IntT         m_si_errno;
    ProcessId    m_si_pid;
    UidT         m_si_uid;
    VoidTPtr     m_si_addr;
    IntT         m_si_status;
    LongT        m_si_band;
    SignalValue* m_si_value;
} SigInfo;


/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/
SigAction* create_SigAction();
Signal* create_Signal(int signo);
CalledFunctionDescription* create_CalledFunctionDescription(int uid, int function_called, int function_nested);
SignalValue* create_SignalValue(bool is_int, IntT int_value, VoidTPtr ptr_value);
SignalParams* create_SignalParams(bool is_int, IntT int_value, VoidTPtr ptr_value, 
                                  CallContext call_context, IntT unique_id, bool returned_kill);
SigInfo* create_SigInfo(VoidTPtr address, IntT m_si_signo, IntT m_si_code, IntT m_si_errno, 
                        ProcessId m_si_pid, UidT m_si_uid, VoidTPtr m_si_addr,
                        IntT m_si_status, LongT m_si_band, SignalValue* m_si_value);


#endif

