/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef COMMON_CSTRING_SEH
#define COMMON_CSTRING_SEH

#include <data/embed_model.seh>

struct _string
{
  CharT* data;
  int    length;
};

#ifdef __SEC__
  specification typedef struct _string CString;
#else
  typedef struct _string CString;
  extern const Type type_CString;
#endif

/*
 * Constructs a newly allocated CString object
 * that represents a null terminated string.
 */
CString* create_CString( CharT* str );

#define _CS(a) (a == NULL ? NULL : create_CString(a))

/*
 * Returns the value of this CString as a CharT*.
 */
CharT* toCharArray_CString( CString* self );

/*
 * Returns the length of this CString.
 */
int length_CString( CString* self );

/*
 * Returns the character at the specified offset from the beginning of this
 * CString.
 */
CharT charAt_CString( CString* self, int offset );

/*
 * Sets the character at the specified offset from the beginning of this
 * CString.
 */
void setCharAt_CString( CString* self, int offset, CharT val );

/*
 * Concatenates the specified string to the end of this string.
 */
CString* concat_CString( CString* self, CString *str );

/*
 * Returns the index within this string of the first occurrence 
 * of the specified character.
 */
int indexOfChar_CString( CString *self, CharT ch );

/*
 * Returns the index within this string of the first occurrence
 * of the specified character, starting the search at the specified index.
 */
int indexOfCharFrom_CString( CString *self, CharT ch, int fromIndex );

/*
 * Returns a new string that is a substring of this string.
 */
CString *substringFrom_CString( CString *self, int beginIndex );

/*
 * Returns a new string that is a substring of this string.
 */
CString *substring_CString( CString *self, int beginIndex, int endIndex );

/*
 * Removes white space from both ends of this string.
 */
CString* trim_CString( CString *self );

/*
 * Returns a new string resulting from replacing all occurrences
 * of old substring in this string with new string.
 */
CString *replace_CString( CString *self, CString* old, CString* new );

/*
 * Format string in sprinrf-manner
 */
CString* format_CString(const char *fmt, ...);

CString *createChar_CString(CharT ch);

CString *concatChar_CString(CString *self, CharT ch);

CString *concatPChar_CString(CString *self, CharT *str);

bool isEmpty_CString(CString* self);
int safeCompare_CString(CString* str1, CString* str2);

#endif

