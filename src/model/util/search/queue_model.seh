/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef UTIL_SEARCH_QUEUE_MODEL_SEH
#define UTIL_SEARCH_QUEUE_MODEL_SEH

#include <atl/list.h>

#include "common/common_model.seh"
#include "config/system_config.seh"
#include <atl/integer.h>

typedef VoidTPtr QueueElementPtr;

specification typedef QueueElementPtr QueueElementPtrObj;

QueueElementPtrObj* create_QueueElementPtrObj(QueueElementPtr ptr);

specification invariant typedef struct QueueElement
{
    QueueElementPtr address;
    QueueElementPtr forward;
    QueueElementPtr backward;
} QueueElement;

invariant typedef List QueueElementList;

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** insque_spec **/
specification
void insque_spec(CallContext context, VoidTPtr element, QueueElementPtr pred);

/** remque_spec **/
specification
void remque_spec(CallContext context, QueueElementPtr element);

specification
void create_circular_queue_spec(CallContext context, VoidTPtr element);

specification
void create_linear_queue_spec(CallContext context, VoidTPtr element);

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

QueueElementList* queue_List(QueueElementPtr address);
QueueElement* getFirstQueueElement(QueueElementPtr address);
int getQueueElementIndex(QueueElementList* list, QueueElementPtr address);

bool isCircularQueue(QueueElementPtr address);
bool isQueueElementAvailable(QueueElementPtr element);
QueueElement* getQueueElement(QueueElementPtr address);
QueueElement* next_QueueElement(QueueElementPtr address);
QueueElement* prev_QueueElement(QueueElementPtr address);

QueueElement* create_QueueElement(VoidTPtr address, VoidTPtr forward, VoidTPtr backward); 
QueueElementList* create_QueueElementList(void);

/********************************************************************/
/**                        Debug Functions                         **/
/********************************************************************/

void printNodeList(List *queue_list);
void printQueue(QueueElement* element);
void printNode(QueueElement* element);

#endif

