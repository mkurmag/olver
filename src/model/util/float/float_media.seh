/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef UTIL_FLOAT_MEDIA_SEH
#define UTIL_FLOAT_MEDIA_SEH

#include "util/float/float_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initUtilFloatSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** __finite_spec **/
//This mediator refers to: __finitef, __finite, __finitel
mediator __finite_media for 
specification
IntT __finite_spec(CallContext context, Unifloat* x);

/** __fpclassify_spec **/
//This mediator refers to: __fpclassifyf, __fpclassify
mediator __fpclassify_media for 
specification
IntT __fpclassify_spec(CallContext context, Unifloat* x);

/** __isinf_spec **/
//This mediator refers to: __isinff, __isinf, __isinfl
mediator __isinf_media for 
specification
IntT __isinf_spec(CallContext context, Unifloat* x);

/** __isnan_spec **/
//This mediator refers to: __isnanf, __isnan, __isnanl
mediator __isnan_media for 
specification
IntT __isnan_spec(CallContext context, Unifloat* x);

/** __signbit_spec **/
//This mediator refers to: __signbitf, __signbit
mediator __signbit_media for 
specification
IntT __signbit_spec(CallContext context, Unifloat* x, IntT *ExistFunction);

/** finite_spec **/
//This mediator refers to: finitef, finite, finitel
mediator finite_media for 
specification
IntT finite_spec(CallContext context, Unifloat* x);

/** frexp_spec **/
//This mediator refers to: frexpf, frexp, frexpl
mediator frexp_media for 
specification
Unifloat* frexp_spec(CallContext context, Unifloat* x, IntT* exp);

/** ilogb_spec **/
//This mediator refers to: ilogbf, ilogb, ilogbl
mediator ilogb_media for 
specification
IntT ilogb_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** ldexp_spec **/
//This mediator refers to: ldexpf, ldexp, ldexpl
mediator ldexp_media for specification
Unifloat* ldexp_spec(CallContext context, Unifloat* x, IntT exp,
                     ErrorCode* errno);

/** logb_spec **/
//This mediator refers to: logbf, logb, logbl
mediator logb_media for 
specification
Unifloat* logb_spec(CallContext context, Unifloat* x, ErrorCode* errno);

/** nextafter_spec **/
//This mediator refers to: nextafterf, nextafter, nextafterl
mediator nextafter_media for 
specification
Unifloat* nextafter_spec(CallContext context, Unifloat* x, Unifloat* y, 
                         ErrorCode* errno);
/** nexttoward_spec **/
//This mediator refers to: nexttowardf, nexttoward, nexttowardl
mediator nexttoward_media for 
specification
Unifloat* nexttoward_spec(CallContext context, Unifloat* x, Unifloat* y, 
                         ErrorCode* errno);

/** scalb_spec **/
//This mediator refers to: scalbf, scalb, scalbl
mediator scalb_media for 
specification
Unifloat* scalb_spec(CallContext context, Unifloat* x, Unifloat* n,
                     ErrorCode* errno);

/** scalbln_spec **/
//This mediator refers to: scalblnf, scalbln, scalblnl
mediator scalbln_media for 
specification
Unifloat* scalbln_spec(CallContext context, Unifloat* x, LongT n,
                       ErrorCode* errno);

/** scalbn_spec **/
//This mediator refers to: scalbnf, scalbn, scalbnl
mediator scalbn_media for 
specification
Unifloat* scalbn_spec(CallContext context, Unifloat* x, IntT n, 
                      ErrorCode* errno);

/** significand_spec **/
//This mediator refers to: significandf, significand, significandl
mediator significand_media for 
specification
Unifloat* significand_spec(CallContext context, Unifloat* x);


#endif

