/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef UTIL_CONVERSION_WCHAR_MEDIA_SEH
#define UTIL_CONVERSION_WCHAR_MEDIA_SEH

#include "util/conversion/wchar_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initUtilConversionWcharSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** btowc_spec **/
mediator btowc_media for specification
WIntT btowc_spec( CallContext context, IntT c);

/** mblen_spec **/
mediator mblen_media for specification
IntT mblen_spec( CallContext context, StringTPtr s, IntT n, ErrorCode* errno);

/** mbrlen_spec **/
mediator mbrlen_media for specification
IntT mbrlen_spec( CallContext context, StringTPtr s, IntT n, Mbstate* ps, ErrorCode* errno);

/** mbrtowc_spec **/
mediator mbrtowc_media for specification
IntT mbrtowc_spec( CallContext context, WStringTPtr* pwc, StringTPtr s, IntT n, Mbstate* ps, ErrorCode* errno);

/** mbsinit_spec **/
mediator mbsinit_media for specification
IntT mbsinit_spec( CallContext context, Mbstate* ps);

/** mbsnrtowcs_spec **/
mediator mbsnrtowcs_media for specification
IntT mbsnrtowcs_spec( CallContext context, WStringTPtr* dst, StringTPtr* src, IntT nms, IntT len, Mbstate* ps, ErrorCode* errno);

/** mbsrtowcs_spec **/
mediator mbsrtowcs_media for specification
IntT mbsrtowcs_spec( CallContext context, WStringTPtr* dst, StringTPtr* src, IntT len, Mbstate* ps, ErrorCode* errno);

/** mbstowcs_spec **/
mediator mbstowcs_media for specification
IntT mbstowcs_spec( CallContext context, WStringTPtr* pwcs, StringTPtr s, IntT n, ErrorCode* errno);

/** mbtowc_spec **/
mediator mbtowc_media for specification
IntT mbtowc_spec( CallContext context, WStringTPtr* pwc, StringTPtr s, IntT n, ErrorCode* errno);

/** wcrtomb_spec **/
mediator wcrtomb_media for specification
IntT wcrtomb_spec( CallContext context, StringTPtr* s, WCharT wc, Mbstate* ps, ErrorCode* errno);

/** wcsnrtombs_spec **/
mediator wcsnrtombs_media for specification
IntT wcsnrtombs_spec( CallContext context, StringTPtr* dst, WStringTPtr* src, IntT nws, IntT len, Mbstate* ps, ErrorCode* errno);

/** wcsrtombs_spec **/
mediator wcsrtombs_media for specification
IntT wcsrtombs_spec( CallContext context, StringTPtr* dst, WStringTPtr* src, IntT len, Mbstate* ps, ErrorCode* errno);

/** wcstombs_spec **/
mediator wcstombs_media for specification
IntT wcstombs_spec( CallContext context, StringTPtr* s, WStringTPtr pwcs, IntT n, ErrorCode* errno);

/** wctob_spec **/
mediator wctob_media for specification
IntT wctob_spec( CallContext context, WIntT c );

/** wctomb_spec **/
mediator wctomb_media for specification
IntT wctomb_spec( CallContext context, StringTPtr* s, WCharT wchar );


void writeMbstate_TSStream( TSStream* stream, Mbstate* value );

static void mbstate_formatter(TSStream* stream,Mbstate* value);

#endif

