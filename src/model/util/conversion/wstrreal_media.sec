/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "util/conversion/wstrreal_media.seh"
#include "util/conversion/wstrreal_model.seh"
#include "common/common_media.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initUtilConversionWstrrealSubsystem(void)
{
  // Set up mediators
    set_mediator___wcstod_internal_spec(__wcstod_internal_media);
    set_mediator_wcstod_spec(wcstod_media);
}


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** __wcstod_internal_spec **/
//This mediator refers to: __wcstof_internal, __wcstod_internal, __wcstold_internal
mediator __wcstod_internal_media for
specification
Unifloat * __wcstod_internal_spec
               ( CallContext context, WString * st, WString ** endptr, UnifloatType type, ErrorCode * errno, IntT group )
{
    call
    {
        TSCommand command = create_TSCommand();
        Unifloat* res;
        IntT shift;
        WCharArray* array = create_WCharArray(toWCharArray_WString(st), length_WString(st));
        char command_string[3][100] = {
                "__wcstof_internal:$(int)$(wchararray)",
                "__wcstod_internal:$(int)$(wchararray)",
                "__wcstold_internal:$(int)$(wchararray)"};

        format_TSCommand( &command, command_string[type], create_IntTObj(st->length),
                          array);

        executeCommandInContext( context, &command );

        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            shift = readInt_TSStream(&command.response);
            *endptr = substring_WString(st, shift, length_WString(st));
            *errno = readInt_TSStream(&command.response);
        }

        destroy_TSCommand(&command);

        return res;
  }
}

/** wcstod_spec **/
//This mediator refers to: wcstof, wcstod, wcstold
mediator wcstod_media for
specification
Unifloat* wcstod_spec(CallContext context, WString* st, WString** endptr,
                      UnifloatType type, ErrorCode* errno)
{
    call
    {
        bool showData = true;

        TSCommand command = create_TSCommand();
        Unifloat* res;
        IntT shift;
        WCharArray* array = create_WCharArray(toWCharArray_WString(st), length_WString(st));
        char command_string[3][100] = {
                "wcstof:$(int)$(wchararray)",
                "wcstod:$(int)$(wchararray)",
                "wcstold:$(int)$(wchararray)"};

        if ( showData ) {
            String * asString = toString( st );
            verbose( "wcstod_media : st   is [%s]\n", toCharArray_String( asString ) );
            verbose( "wcstod_media : type is [%i]\n", type                           );
        }

        format_TSCommand( &command, command_string[type], create_IntTObj(st->length),
                          array);

        executeCommandInContext( context, &command );

        if (!isBadVerdict())
        {
            timestamp = command.meta.timestamp;
            res = readUnifloat_TSStream(&command.response);
            shift = readInt_TSStream(&command.response);
            *endptr = substring_WString(st, shift, length_WString(st));
            *errno = readInt_TSStream(&command.response);
        }

        if ( showData ) {
            String * t;
            t = toString( res      );    verbose( "wcstod_media : res      is [%s]\n", toCharArray_String( t ) );
            t = toString( * endptr );    verbose( "wcstod_media : * endptr is [%s]\n", toCharArray_String( t ) );
            t = toString( errno    );    verbose( "wcstod_media : errno    is [%s]\n", toCharArray_String( t ) );
        }

        destroy_TSCommand(&command);

        return res;
  }
}



