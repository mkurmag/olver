/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef UTIL_CONVERSION_TOKEN_MEDIA_SEH
#define UTIL_CONVERSION_TOKEN_MEDIA_SEH

#include "util/conversion/token_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initUtilConversionTokenSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/
/** strsep_spec **/
mediator strsep_media for specification
StrSepResult* strsep_spec( CallContext context, StringTPtr stringp,
                                                            StringTPtr delim);
/** strtok_spec **/
mediator strtok_media for specification
StringTPtr strtok_spec( CallContext context, StringTPtr stringp,
                                                            StringTPtr delim);

/** strtok_r_spec **/
mediator strtok_r_media for specification
StringTPtr strtok_r_spec( CallContext context, StringTPtr stringp,
                                        StringTPtr delim, StringTPtr* buffer);

/** __strtok_r_spec **/
mediator __strtok_r_media for specification
StringTPtr __strtok_r_spec( CallContext context, StringTPtr stringp,
                                        StringTPtr delim, StringTPtr* buffer);
#endif

