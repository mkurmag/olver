/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "util/conversion/tests/strint_scenario.seh"


#include "util/conversion/strint_model.seh"
#include "config/system_config.seh"
#include "common/common_media.seh"
#include "common/common_scenario.seh"

/********************************************************************/
/**                     Test Scenario Parameters                   **/
/********************************************************************/
static char* strint_data[]=
{
     " 0   ",//0
     " +0   ",
     " -0   ",

     " 1   ",//3
     " +1   ",
     " -1   ",

     " 2147483647   ",//6
     " +2147483647   ",
     " -2147483647   ",

     " 2147483650   ",//9
     " +2147483648   ",
     " -2147483648   ",

     " x   ",//12
     " +x   ",
     " -x   ",

     " 0x   ",//15
     " +0x   ",
     " -0x  ",

     " 0xz   ",//18
     " +0xz   ",
     " -0xz  ",

     "   0123456789erwerdsfsdfds",//21
     "   +0123456789erwerdsfsdfds",
     "   -0123456789erwerdsfsdfds",

     "  123 sdfsdfs",//24
     "  +123 sdfsdfs",
     "  -123 sdfsdfs",

     "  0X123  sfsdfsdfs",//27
     "  +0X123  sfsdfsdfs",
     "  -0X123  sfsdfsdfs",

     "qwerty",//30
     "+qwerty",
     "-qwerty",

     " 2000000zzz",//33
     " +2000000zzz",
     " -2000000zzz",

     " 1vvvvvvzzz",//36
     " +1vvvvvvzzz",
     " -1vvvvvvzzz",

     " 0x7FFFFFFF",//39
     " +0x7FFFFFFF",
     " -0x7FFFFFFF",

     " 0x80000000",//42
     " +0x80000000",
     " -0x80000000",

     " 0x7FFFFFFFFFFFFFFF",//45
     " +0x7FFFFFFFFFFFFFFF",
     " -0x7FFFFFFFFFFFFFFF",

     " 0x8000000000000000",//48
     " +0x8000000000000000",
     " -0x8000000000000000",

     " 9223372036854775808 ",
     " +9223372036854775808 ",
     " -9223372036854775808 ",

     " 9223372036854775807 ",
     " +9223372036854775807 ",
     " -9223372036854775807 "
};
static LongT l64a_arr[]=
{
    0,
    1,
    100,
    65535,
    1000000000,
    2147483647
};
static int strint_base[]=
{
    0,
    2,
    7,
    8,
    16,
    21,
    32,
    36
};
static int strint_data_size=sizeof(strint_data)/sizeof(wchar_t*);
static int strint_base_size=sizeof(strint_base)/sizeof(int);
static int l64a_size=sizeof(l64a_arr)/sizeof(LongT);
static Set* st;
static const int maxSize=1024;
/********************************************************************/
/**                       Test Scenario Data                       **/
/********************************************************************/
static CallContext context;
static VoidTPtr nptr;
static VoidTPtr endptr;
/********************************************************************/
/**                  Test Scenario Initialization                  **/
/********************************************************************/

static bool init_strint_scenario(int argc, char** argv)
{
    // Init test scenario data
    context = getContext();
    nptr = allocateMemoryBlock(context, maxSize);
    st = create_Set(&type_CString);
    initReqFilters();

    return true;
}
static void finish_strint_scenario(void)
{
    if(!isNULL_VoidTPtr(nptr))
        deallocateMemoryBlock(context, nptr);

    TEST_SCENARIO_VERDICT_VERBOSE(util_conversion_strint_scenario);
}
/********************************************************************/
/**                          Test Actions                          **/
/********************************************************************/
#define myscen strtol_scen
#define myspec strtol_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen strtoll_scen
#define myspec strtoll_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen __strtol_internal_scen
#define myspec __strtol_internal_spec
#define ParameterGroupExists
#include "strto_scen.seh"
#undef myscen
#undef myspec
#undef ParameterGroupExists

#define myscen __strtoll_internal_scen
#define myspec __strtoll_internal_spec
#define ParameterGroupExists
#include "strto_scen.seh"
#undef myscen
#undef myspec
#undef ParameterGroupExists

#define myscen strtoq_scen
#define myspec strtoq_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen strtoimax_scen
#define myspec strtoimax_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen strtoul_scen
#define myspec strtoul_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen strtoull_scen
#define myspec strtoull_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen __strtoul_internal_scen
#define myspec __strtoul_internal_spec
#define ParameterGroupExists
#include "strto_scen.seh"
#undef myscen
#undef myspec
#undef ParameterGroupExists

#define myscen __strtoull_internal_scen
#define myspec __strtoull_internal_spec
#define ParameterGroupExists
#include "strto_scen.seh"
#undef myscen
#undef myspec
#undef ParameterGroupExists

#define myscen strtouq_scen
#define myspec strtouq_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen strtoumax_scen
#define myspec strtoumax_spec
#include "strto_scen.seh"
#undef myscen
#undef myspec

#define myscen atoi_scen
#define myspec atoi_spec
#include "ato_scen.seh"
#undef myscen
#undef myspec

#define myscen atol_scen
#define myspec atol_spec
#include "ato_scen.seh"
#undef myscen
#undef myspec

#define myscen atoll_scen
#define myspec atoll_spec
#include "ato_scen.seh"
#undef myscen
#undef myspec


scenario
bool l64a_scen()
{
    CString* str;
    iterate(int i=0;i<l64a_size;i++;)
    {
        str=clone(l64a_spec(context, l64a_arr[i]));
        DUMP("Adding in set: $(obj)\n", str);
        add_Set(st, str);
    }

    return true;
}

scenario
bool a64l_scen()
{
    CString* strint_str;
    LongT ret;

    iterate(int i=0;i<size_Set(st);i++;)
    {
        strint_str=clone(get_Set(st, i));
        DUMP("Getting from set==$(obj)(end_of_line)\n", strint_str);
        ret=a64l_spec(context, strint_str);

        if(ret!=l64a_arr[i])
            DUMP("ERROR: for iteration i==%d: %ld!=%ld\n", i, ret, l64a_arr[i]);
    }

    return true;
}
/********************************************************************/
/**                    Test Scenario Definition                    **/
/********************************************************************/
scenario dfsm util_conversion_strint_scenario =
{
    .init = init_strint_scenario,
    .finish = finish_strint_scenario,
    .actions = {
//Strto
//Signed
                    strtol_scen,
                    strtoll_scen,
                    __strtol_internal_scen,
                    __strtoll_internal_scen,
                    strtoq_scen,
                    strtoimax_scen,

//Unsigned
                    strtoul_scen,
                    strtoull_scen,
                    __strtoul_internal_scen,
                    __strtoull_internal_scen,
                    strtouq_scen,
                    strtoumax_scen,
//Ato
                    atoi_scen,
                    atol_scen,
                    atoll_scen,

//64
                    l64a_scen,
                    a64l_scen,

                    NULL
                }
};

#include "util/conversion/tests/strint_scenario.seh"

bool main_util_conversion_strint(int argc, char** argv)
{
    util_conversion_strint_scenario(argc,argv);
    return true;
}


#ifdef UTIL_CONVERSION_STRINT_LOCAL_MAIN

#include "common/init.seh"

#include "common/common_media.seh"
#include "common/common_scenario.seh"
#include "common/control_center.seh"
#include "config/system_config.seh"
#include "system/system/system_model.seh"
#include "process/process/process_model.seh"
#include "pthread/pthread/pthread_media.seh"
#include "pthread/mutex/mutexattr_media.seh"
#include "pthread/mutex/mutex_media.seh"
#include "util/conversion/strint_media.seh"


/********************************************************************/
/**                     Test System Initialization                 **/
/********************************************************************/
void reinitTestSystem(void)
{
    reinitControlCenter();
    initCommonModel();
    initCommonMedia();
    initCommonScenarioState();

    initSystemConfiguration();
    initSystemModel();
    initProcessModel();

    initUtilConversionStrintSubsystem();
}

int main(int argc, char** argv)
{
    //
    initTestSystem();
    loadSUT();
    // Set up tracer
    setTraceEncoding("windows-1251");

    addTraceToFile("trace.xml");

    // Run test scenario
    main_util_conversion_strint(argc,argv);

    //  unloadSUT();
    return 0;
}
#endif
