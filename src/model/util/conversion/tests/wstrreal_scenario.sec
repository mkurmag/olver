/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "util/conversion/tests/wstrreal_scenario.seh"
#include "util/conversion/wstrreal_media.seh"
#include "config/system_config.seh"
#include "common/common_media.seh"
#include "common/common_scenario.seh"
#include "math/math/math_data.seh"

#include <stdio.h>

/********************************************************************/
/**                     Test Scenario Parameters                   **/
/********************************************************************/

/********************************************************************/
/**                       Test Scenario Data                       **/
/********************************************************************/
static CallContext context;
static VoidTPtr ptr;
/********************************************************************/
/**                  Test Scenario Initialization                  **/
/********************************************************************/
static bool init_util_conversion_wstrreal(int argc, char** argv)
{
    // Init test scenario data
    context = getContext();

    initReqFilters();

    ptr = allocateMemoryBlock(context, 1024);

    setFinishMode(UNTIL_END);

    return true;
}
static void finish_conversion_wstrreal(void)
{
    deallocateMemoryBlock(context, ptr);
    TEST_SCENARIO_VERDICT_VERBOSE(util_conversion_wstrreal_scenario);
}

/********************************************************************/
/**                          Test Actions                          **/
/********************************************************************/
scenario bool __wcstod_internal_wcstod_scen()
{
    WString* endptr;
//    IntT type, i;
    WString* st[30] = {
    createFromCharArray_WString("-inf"),
    createFromCharArray_WString("         iNfInItY-NaN"),
    createFromCharArray_WString("  \n \t     -infFinity"),
    createFromCharArray_WString("-ininity"),
    createFromCharArray_WString("InfinityThis stoped scan"),
    createFromCharArray_WString("NaN"),
    createFromCharArray_WString("       NaN(123)"),
    createFromCharArray_WString("\n   \t -NaN(1ab3cD)This stoped scan"),
    createFromCharArray_WString("NaN()The end."),
    createFromCharArray_WString("-NaN("),
    createFromCharArray_WString("\nNaN(Hello world) - Hello!"),
    createFromCharArray_WString(" \n 123.456.789"),
    createFromCharArray_WString(".."),
    createFromCharArray_WString("       .1"),
    createFromCharArray_WString("-.1234567890"),
    createFromCharArray_WString(" -987.654This stoped scan"),
    createFromCharArray_WString("1e10"),
    createFromCharArray_WString("4.55e-5With negativ exponent"),
    createFromCharArray_WString("12."),
    createFromCharArray_WString("-123e"),
    createFromCharArray_WString("456eBad exponent"),
    createFromCharArray_WString("123e-"),
    createFromCharArray_WString(" NaN(123"),
    createFromCharArray_WString("No number"),
    createFromCharArray_WString("1e-12345"),
    createFromCharArray_WString("-1e12345"),
    createFromCharArray_WString("\n---------Hello--------"),
    createFromCharArray_WString("123e-end"),
    createFromCharArray_WString("234e5.2"),
    createFromCharArray_WString("e5")
    };
    iterate ( IntT type = 0; type < 3; type++; )
        iterate ( IntT i = 0; i < 30; i++; )
        {
            __wcstod_internal_spec( context, st[ i ], & endptr, type, requestErrorCode(), 0 );
            wcstod_spec(context, st[i], &endptr, type, requestErrorCode());
        }
    return true;
}

/********************************************************************/
/**                    Test Scenario Definition                    **/
/********************************************************************/
scenario dfsm util_conversion_wstrreal_scenario =
{
    .init = init_util_conversion_wstrreal,
    .finish = finish_conversion_wstrreal,
    .actions = {
                    __wcstod_internal_wcstod_scen,
                    NULL
                }
};

bool main_util_conversion_wstrreal(int argc, char** argv)
{
    util_conversion_wstrreal_scenario(argc,argv);
    return true;
}

#include "util/conversion/tests/wstrreal_scenario.seh"

#ifdef UTIL_CONVERSION_WSTRREAL_LOCAL_MAIN

#include "common/init.seh"

#include "common/common_media.seh"
#include "common/common_scenario.seh"
#include "common/control_center.seh"
#include "config/system_config.seh"
#include "system/system/system_model.seh"
#include "process/process/process_model.seh"
#include "pthread/pthread/pthread_media.seh"
#include "pthread/mutex/mutexattr_media.seh"
#include "pthread/mutex/mutex_media.seh"
#include "util/conversion/wstrreal_media.seh"


/********************************************************************/
/**                     Test System Initialization                 **/
/********************************************************************/
void reinitTestSystem(void)
{
    reinitControlCenter();
    initCommonModel();
    initCommonMedia();
    initCommonScenarioState();

    initSystemConfiguration();
    initSystemModel();
    initProcessModel();
    initMath();

    initUtilConversionWstrrealSubsystem();
}

#ifdef WIN32

int main(int argc, char** argv)
{
    clock_t start, finish;
    double duration;

    initTestSystem();

    loadSUT();

    // Set up tracer
    setTraceEncoding("windows-1251");

    // Run test scenario
    addTraceToFile("trace.xml");


    start = clock();

    main_util_conversion_wstrreal(argc,argv);


    finish = clock();
    duration = (double) (finish - start) / CLOCKS_PER_SEC;

    verbose( "%2.3f seconds\n", duration );



    //  unloadSUT();
    return 0;
}

#else

int main(int argc, char** argv)
{
    initTestSystem();
    loadSUT();

    // Set up tracer
    setTraceEncoding("windows-1251");

    // Run test scenario
    addTraceToFile("trace.xml");
    main_util_conversion_wstrreal(argc,argv);

    //  unloadSUT();
    return 0;
}

#endif

#endif





