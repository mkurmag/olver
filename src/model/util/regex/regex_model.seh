/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UTIL_REGEX_MODEL_SEH
#define UTIL_REGEX_MODEL_SEH

#include "common/common_model.seh"

bool knownErrcode( IntT e );

/********************************************************************/
/**                      Definitions                               **/
/********************************************************************/

typedef VoidTPtr RegexTPtr;

#define SUT_FNM_PATHNAME    (1<<0)
#define SUT_FNM_NOESCAPE    (1<<1)
#define SUT_FNM_PERIOD    (1<<2)
#define SUT_FNM_NOMATCH    1

#define SUT_REG_EXTENDED    1
#define SUT_REG_ICASE        (SUT_REG_EXTENDED<<1)
#define SUT_REG_NEWLINE        (SUT_REG_ICASE<<1)
#define SUT_REG_NOSUB        (SUT_REG_NEWLINE<<1)

#define SUT_REG_NOTEOL        (1<<1)
#define SUT_REG_NOTBOL        1

typedef enum {
    SUT_REG_ENOSYS, SUT_REG_NOERROR, SUT_REG_NOMATCH, SUT_REG_BADPAT, SUT_REG_ECOLLATE,
    SUT_REG_ECTYPE, SUT_REG_EESCAPE, SUT_REG_ESUBREG, SUT_REG_EBRACK, SUT_REG_EPAREN,
    SUT_REG_EBRACE, SUT_REG_BADBR, SUT_REG_ERANGE, SUT_REG_ESPACE, SUT_REG_BADRPT,
    SUT_REG_EEND, SUT_REG_ESIZE, SUT_REG_ERPAREN
} RegErrcodeT;

typedef IntT RegoffT;

/********************************************************************/
/**                      RegmatchT Data Type                       **/
/********************************************************************/

specification typedef struct RegmatchT {
    RegoffT   rm_so; // Byte offset from start of string to start of substring.
    RegoffT   rm_eo; // Byte offset from start of string of the first character after the end of substring.
} RegmatchT;

RegmatchT * create_RegmatchT( RegoffT rm_so, RegoffT rm_eo );

/********************************************************************/
/**                      Helper Data Types                         **/
/********************************************************************/

specification typedef struct RegcompReturnType { IntT funcRes; RegexTPtr preg; } RegcompReturnType;

RegcompReturnType * create_RegcompReturnType( IntT funcRes, RegexTPtr preg );

specification typedef struct RegexecReturnType { IntT funcRes; List /* RegmatchT */ * pmatch; } RegexecReturnType;

RegexecReturnType * create_RegexecReturnType( IntT funcRes, List /* RegmatchT */ * pmatch );

specification typedef struct RegerrorReturnType { SizeT funcRes; CString * errbuf; } RegerrorReturnType;

RegerrorReturnType * create_RegerrorReturnType( SizeT funcRes, CString * errbuf );

/********************************************************************/
/**                          RegExp Data                           **/
/********************************************************************/

specification typedef struct CompiledRegularExpression { // private:
    RegexTPtr   preg                         ;
// public:
    CString   * pattern                      ;
    bool        useExtendedRegularExpressions;
    bool        ignoreCaseInMatch            ;
    bool        reportOnlySuccessFail        ;
    bool        changeNewlinesHandling       ;
#if 0 // for avoid too many "// "
public:
    bool correspondTo( RegexTPtr preg ) const;
#endif // 0 for avoid too many "// "
} CompiledRegularExpression;

/* quasi constructor */
CompiledRegularExpression * create_CompiledRegularExpression( RegexTPtr preg, CString * pattern, IntT cflags );

bool correspondTo_CompiledRegularExpression
         ( /* const */ CompiledRegularExpression * thisCompiledRegularExpression, RegexTPtr preg );

specification typedef struct CompiledRegularExpressions { // private:
    Set /* CompiledRegularExpression */ * compiledRegularExpressions;
#if 0 // for avoid too many "// "
public:
    static CompiledRegularExpressions * fromObject( Object * compiledRegularExpressionsAsObject );
    static CompiledRegularExpressions * fromCallContext( CallContext context );
    CompiledRegularExpression * find( RegexTPtr preg );
    void add( const CompiledRegularExpression * compiledRegularExpression );
    void remove( RegexTPtr preg );
#endif // 0 for avoid too many "// "
} CompiledRegularExpressions;

/* quasi constructor */
CompiledRegularExpressions * create_CompiledRegularExpressions( void );

CompiledRegularExpressions * fromObject_CompiledRegularExpressions( Object * compiledRegularExpressionsAsObject );
CompiledRegularExpressions * fromCallContext_CompiledRegularExpressions( CallContext context );
CompiledRegularExpression * find_CompiledRegularExpressions
                                ( CompiledRegularExpressions * thisCompiledRegularExpressions, RegexTPtr preg );
void add_CompiledRegularExpressions( CompiledRegularExpressions * thisCompiledRegularExpressions,
                                     /* const */ CompiledRegularExpression * compiledRegularExpression
                                   );
void remove_CompiledRegularExpressions( CompiledRegularExpressions * thisCompiledRegularExpressions, RegexTPtr preg );

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** fnmatch_spec **/
specification
IntT fnmatch_spec( CallContext context, CString * pattern, CString * str, IntT flags );

/** regcomp_spec **/
specification
RegcompReturnType * regcomp_spec( CallContext context, CString * pattern, IntT cflags );

/** regerror_spec **/
specification
RegerrorReturnType * regerror_spec( CallContext context, IntT errcode, RegexTPtr preg, SizeT errbuf_size );

/** regexec_spec **/
specification
RegexecReturnType * regexec_spec( CallContext context, RegexTPtr preg, CString * str, SizeT nmatch, IntT eflags );

/** regfree_spec **/
specification
void regfree_spec( CallContext context, RegexTPtr preg );

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

specification typedef struct FnmatchSheaf { // data
                                            CString              * pattern  ;
                                            CString              * whereFind;
                                            IntT                   flags    ;
                                            // expected result
                                            IntT                   res      ;
                                            // covered reqs
                                            List /* String    */ * reqIds   ;
                                          } FnmatchSheaf;

/* ... == "reqId1", ..., "reqIdn", NULL
 * n >= 0
 * minimal ... == NULL
 */
FnmatchSheaf * create_FnmatchSheaf( char * pattern, char * whereFind, IntT flags, IntT res, ... );

extern FnmatchSheaf * currentFnmatchSheaf;

bool checkFnmatchResult( CString * pattern, CString * str, IntT flags, IntT res );

specification typedef struct RegexpSheaf { // data
                                           CString              * pattern   ;
                                           IntT                   cflags    ;
                                           // expected result
                                           IntT                   regcompRes;
                                           // data
                                           CString              * whereFind ;
                                           SizeT                  nmatch    ;
                                           IntT                   eflags    ;
                                           // expected result
                                           IntT                   regexecRes;
                                           List /* RegmatchT */ * pmatch    ;
                                           // covered reqs
                                           List /* String    */ * reqIds    ;
                                         } RegexpSheaf;

/* ... == rm_so_1, rm_eo_1, ..., rm_so_m, rm_eo_m, -1, "reqId1", ..., "reqIdn", NULL
 * m >= 0, n >= 0
 * minimal ... == -1, NULL
 */
RegexpSheaf * create_RegexpSheaf( char * pattern, IntT cflags, IntT regcompRes,
                                  char * whereFind, SizeT nmatch, IntT eflags, IntT regexecRes, ...
                                );

extern RegexpSheaf * currentSheaf;

bool checkRegcompResult( CString * pattern, IntT cflags, IntT regcompRes );

bool checkRegexecResult( CString * str, SizeT nmatch, IntT eflags, IntT regexecRes, List /* RegmatchT */ * pmatch );

#endif
