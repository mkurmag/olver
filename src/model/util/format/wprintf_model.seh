/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UTIL_FORMAT_WPRINTF_MODEL_SEH
#define UTIL_FORMAT_WPRINTF_MODEL_SEH

#include "common/common_model.seh"
#include "process/process/process_common.seh"

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** fwprintf_spec **/
specification
IntT fwprintf_spec( CallContext context, FILETPtr * stream, WString * format, List /* NULL */ * arguments,
                    ErrorCode * errno, CString * fileForStreamName, CString * fileForStreamMode
                  );

/** swprintf_spec **/
specification
IntT swprintf_spec
         ( CallContext context, WString * ws, SizeT n, WString * format, List /* NULL */ * arguments, ErrorCode * errno );

/** vfwprintf_spec **/
specification
IntT vfwprintf_spec( CallContext context, FILETPtr * stream, WString * format, List /* NULL */ * arguments,
                     ErrorCode * errno, CString * fileForStreamName, CString * fileForStreamMode
                   );

/** vswprintf_spec **/
specification
IntT vswprintf_spec
         ( CallContext context, WString * ws, SizeT n, WString * format, List /* NULL */ * arguments, ErrorCode * errno );

/** vwprintf_spec **/
specification
IntT vwprintf_spec
         ( CallContext context, WString * format, List /* NULL */ * arguments, WString * output, ErrorCode * errno );

/** wprintf_spec **/
specification
IntT wprintf_spec
         ( CallContext context, WString * format, List /* NULL */ * arguments, WString * output, ErrorCode * errno );

/********************************************************************/
/**                       Helper Functions                         **/
/********************************************************************/

bool checkWprintfPreconditions( char * funcName, WString * format, List /* NULL */ * arguments );

bool checkWprintfResult( char * funcName, WString * format, List /* NULL */ * arguments,
                         IntT returnValue, WString * output, ErrorCode * errno
                       );

#endif
