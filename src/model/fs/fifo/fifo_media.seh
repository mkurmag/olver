/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef FS_FIFO_MEDIA_SEH
#define FS_FIFO_MEDIA_SEH

#include "fs/fifo/fifo_model.seh"


/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/
void initFsFifoSubsystem(void);


/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** mkfifo_spec **/
mediator mkfifo_media for specification
IntT mkfifo_spec( CallContext context, CString* path, FilePermissions* mode, ErrorCode* errno);


/** close_fifo_spec **/
mediator close_fifo_media for specification
IntT close_fifo_spec( CallContext context,  FileDescId  fildes, ErrorCode* errno);


/** write_fifo_spec **/
mediator write_fifo_media for specification
void write_fifo_spec(CallContext context, FileDescId fildes, VoidTPtr buf, 
                     SizeT nbyte, ErrorCode* errno);

mediator write_fifo_return_media for
reaction WriteFIFOReturnType* write_fifo_return(void);


/** read_fifo_spec **/
mediator read_fifo_media for specification
void read_fifo_spec(CallContext context, FileDescId fildes, VoidTPtr buf, 
                    SizeT nbyte, ErrorCode* errno);

mediator read_fifo_return_media for
reaction ReadFIFOReturnType* read_fifo_return(void);

/** writev_fifo_spec **/
mediator writev_fifo_media for specification
void writev_fifo_spec(CallContext context, FileDescId fildes, List* iov, 
                      ErrorCode* errno);

mediator writev_fifo_return_media for
reaction WriteFIFOReturnType* writev_fifo_return(void);

/** readv_fifo_spec **/
mediator readv_fifo_media for specification
void readv_fifo_spec(CallContext context, FileDescId fildes, List* iov, 
                     ErrorCode* errno);

mediator readv_fifo_return_media for
reaction ReadFIFOReturnType* readv_fifo_return(void);

/** open_fifo_spec **/
//This mediator refers to: open_fifo, open64_fifo
mediator open_fifo_media for specification
void open_fifo_spec(CallContext context, CString* path, OpenFlags oflag, 
               FilePermissions* mode, ErrorCode* errno, bool is64bits);

mediator  open_fifo_return_media for
reaction OpenFIFOReturnType* open_fifo_return(void);

#endif

