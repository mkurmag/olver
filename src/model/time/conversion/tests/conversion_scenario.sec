  /*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "system/sysconf/sysconf_media.seh"
#include "system/user/user_media.seh"
#include "process/meta/user_media.seh"
#include "time/conversion/tests/conversion_scenario.seh"
#include "time/conversion/conversion_model.seh"
#include "time/conversion/conversion_media.seh"
#include "process/process/process_common.seh"
#include "config/test_system_config.h"

/********************************************************************/
/**                     Test Scenario Parameters                   **/
/********************************************************************/

bool needOnlyFirstPriority;
bool needConformanceTestQuality;

/********************************************************************/
/**                       Test Scenario Data                       **/
/********************************************************************/

List /* NULL */ * functionData;

static TimeT timeTValues[] = { 0, 1000, 20000, 200000, 2147483647 };

static int len_timeTValues = sizeof( timeTValues ) / sizeof( TimeT );

/********************************************************************/
/**                      Helper Functions                          **/
/********************************************************************/

static List * createContexts( void ) { return createProcesses( true ); }

/********************************************************************/
/**                  Test Scenario Initialization                  **/
/********************************************************************/

String * testingFuncName;

static bool init_scenario( char * name )
{
    //needOnlyFirstPriority = false;
    needOnlyFirstPriority = true;
    needConformanceTestQuality = ( TEST_QUALITY_LEVEL == CONFORMANCE_TEST_QUALITY_LEVEL );
    verbose( "%s\n", name );
    dotPrint( 0, 0 );
    testingFuncName = create_String( name );
    if ( ( functionData = createContexts() ) == NULL ) { return false; }
    setFinishMode( UNTIL_END );
    return true;
}

#define finish_scenario( name ) \
{ \
    verbose( "\n" ); \
    TEST_SCENARIO_VERDICT_VERBOSE( name##_scenario ); \
}

static bool init_conversion_scenario( int argc, char ** argv ) { return init_scenario( "conversion" ); }

static void finish_conversion_scenario( void ) finish_scenario( time )

/********************************************************************/
/**                          Test Actions                          **/
/********************************************************************/

scenario
bool asctime_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timeptrI = 1; timeptrI <= 3; timeptrI++; )
        {
            TmTObj * timeptr;
            switch ( timeptrI ) {
                case 1: timeptr = create_TmTObj( 0, 0, 0, 0, 0, 0, 0, 0, 0 ); break;
                case 2: timeptr = create_TmTObj( 1, 1, 1, 1, 1, 1, 1, 1, 1 ); break;
                case 3: timeptr = create_TmTObj( 9, 9, 9, 9, 9, 9, 9, 9, 9 ); break;
            }
            dotPrint( 10, 100 );
            asctime_spec( * contextObj, timeptr );
        } // iterate timeptrI
    } // iterate contextI
    return true;
}

scenario
bool asctime_r_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timeptrI = 1; timeptrI <= 3; timeptrI++; )
        {
            TmTObj * timeptr;
            switch ( timeptrI ) {
                case 1: timeptr = create_TmTObj( 0, 0, 0, 0, 0, 0, 0, 0, 0 ); break;
                case 2: timeptr = create_TmTObj( 1, 1, 1, 1, 1, 1, 1, 1, 1 ); break;
                case 3: timeptr = create_TmTObj( 9, 9, 9, 9, 9, 9, 9, 9, 9 ); break;
            }
            iterate ( int bufI = 1; bufI <= 2; bufI++; )
            {
                CString * buf;
                switch ( bufI ) {
                    case 1: buf = create_CString( "1234567890123456789012345"  ); break; // 25 + 1
                    case 2: buf = create_CString( "12345678901234567890123456" ); break; // 26 + 1
                }
                dotPrint( 10, 100 );
                asctime_r_spec( * contextObj, timeptr, buf );
            } // iterate bufI
        } // iterate timeptrI
    } // iterate contextI
    return true;
}

scenario
bool ctime_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int clockI = 0; clockI < len_timeTValues; clockI++; )
        {
            TimeTObj * clock = create_TimeTObj( timeTValues[ clockI ] );
            dotPrint( 10, 100 );
            ctime_spec( * contextObj, clock );
        } // iterate clockI
    } // iterate contextI
    return true;
}

scenario
bool ctime_r_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int clockI = 0; clockI < len_timeTValues; clockI++; )
        {
            TimeTObj * clock = create_TimeTObj( timeTValues[ clockI ] );
            iterate ( int bufI = 1; bufI <= 2; bufI++; )
            {
                CString * buf;
                switch ( bufI ) {
                    case 1: buf = create_CString( "1234567890123456789012345"  ); break; // 25 + 1
                    case 2: buf = create_CString( "12345678901234567890123456" ); break; // 26 + 1
                }
                dotPrint( 10, 100 );
                ctime_r_spec( * contextObj, clock, buf );
            } // iterate bufI
        } // iterate clockI
    } // iterate contextI
    return true;
}

scenario
bool gmtime_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timerI = 0; timerI < len_timeTValues; timerI++; )
        {
            TimeTObj * timer = create_TimeTObj( timeTValues[ timerI ] );
            dotPrint( 10, 100 );
            gmtime_spec( * contextObj, timer, requestErrorCode() );
        } // iterate timerI
    } // iterate contextI
    return true;
}

scenario
bool gmtime_r_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timerI = 0; timerI < len_timeTValues; timerI++; )
        {
            TimeTObj * timer = create_TimeTObj( timeTValues[ timerI ] );
            dotPrint( 10, 100 );
            gmtime_r_spec( * contextObj, timer, create_TmTObj( 0, 0, 0, 0, 0, 0, 0, 0, 0 ), requestErrorCode() );
        } // iterate timerI
    } // iterate contextI
    return true;
}

scenario
bool localtime_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timerI = 0; timerI < len_timeTValues; timerI++; )
        {
            TimeTObj * timer = create_TimeTObj( timeTValues[ timerI ] );
            dotPrint( 10, 100 );
            localtime_spec( * contextObj, timer, requestErrorCode() );
        } // iterate timerI
    } // iterate contextI
    return true;
}

scenario
bool localtime_r_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timerI = 0; timerI < len_timeTValues; timerI++; )
        {
            TimeTObj * timer = create_TimeTObj( timeTValues[ timerI ] );
            dotPrint( 10, 100 );
            localtime_r_spec( * contextObj, timer, create_TmTObj( 0, 0, 0, 0, 0, 0, 0, 0, 0 ), requestErrorCode() );
        } // iterate timerI
    } // iterate contextI
    return true;
}

scenario
bool mktime_scen()
{
    int maxContextI = ( needOnlyFirstPriority || needConformanceTestQuality ? 0 : size_List( functionData ) - 1 );
    iterate ( int contextI = 0; contextI <= maxContextI; contextI++; )
    {
        ThreadIdObj * contextObj = get_List( functionData, contextI );
        iterate ( int timeptrI = 1; timeptrI <= 2; timeptrI++; )
        {
            TmTObj * timeptr;
            switch ( timeptrI ) {
                case 1: timeptr = create_TmTObj(  20, 33, 8, 1, 0, 70, 4, 0, 0 ); break;
                case 2: timeptr = create_TmTObj(  40, 16, 3, 1, 0, 70, 4, 0, 0 ); break;
            }
            dotPrint( 10, 100 );
            mktime_spec( * contextObj, timeptr, requestErrorCode() );
        } // iterate timeptrI
    } // iterate contextI
    return true;
}

/********************************************************************/
/**                    Test Scenario Definition                    **/
/********************************************************************/

scenario dfsm time_conversion_scenario =
{
    .init              = init_conversion_scenario  ,
    .finish            = finish_conversion_scenario,
    .actions           = { asctime_scen    ,
                           asctime_r_scen  ,
                           ctime_scen      ,
                           ctime_r_scen    ,
                           gmtime_scen     ,
                           gmtime_r_scen   ,
                           localtime_scen  ,
                           localtime_r_scen,
                           mktime_scen     ,
                           NULL
                         }
};

#ifdef TIME_CONVERSION_LOCAL_MAIN

#include "common/init.seh"

#include "common/common_media.seh"
#include "common/common_scenario.seh"
#include "common/control_center.seh"
#include "config/system_config.seh"
#include "system/system/system_model.seh"
#include "process/process/process_model.seh"
#include "pthread/pthread/pthread_media.seh"
#include "pthread/mutex/mutexattr_media.seh"
#include "pthread/mutex/mutex_media.seh"
#include "process/process/process_media.seh"

/********************************************************************/
/**                     Test System Initialization                 **/
/********************************************************************/

void reinitTestSystem(void)
{
    reinitControlCenter();
    initCommonModel();
    initCommonMedia();
    initCommonScenarioState();

    initSystemConfiguration();
    initSystemModel();
    initProcessModel();
    initPThreadModel();

    initSystemSysconfSubsystem();
    initSystemUserSubsystem();
    initProcessMetaUserSubsystem();

    initTimeConversionSubsystem();
}

bool time_conversion_local( int argc, char ** argv )
{
    time_conversion_scenario( argc, argv );
    return true;
}

int main( int argc, char ** argv )
{
    //
    initTestSystem();
    loadSUT();

    // Set up tracer
    //setTraceEncoding("windows-1251");

    addTraceToFile( "trace.xml_CTesK" );

    // Run test scenario
    time_conversion_local( argc, argv );

    //  unloadSUT();
    return 0;
}

#endif
