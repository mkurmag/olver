/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TIME_CONVERSION_MEDIA_SEH
#define TIME_CONVERSION_MEDIA_SEH

#include "time/conversion/conversion_model.seh"

/********************************************************************/
/**                    Initialization Function                     **/
/********************************************************************/

void initTimeConversionSubsystem(void);

/********************************************************************/
/**                      Interface Functions                       **/
/********************************************************************/

/** asctime_spec **/
mediator asctime_media for specification
CString * asctime_spec( CallContext context, TmTObj * timeptr );

/** asctime_r_spec **/
mediator asctime_r_media for specification
CString * asctime_r_spec( CallContext context, TmTObj * timeptr, CString * buf );

/** ctime_spec **/
mediator ctime_media for specification
CString * ctime_spec( CallContext context, TimeTObj * clock );

/** ctime_r_spec **/
mediator ctime_r_media for specification
CString * ctime_r_spec( CallContext context, TimeTObj * clock, CString * buf );

/** gmtime_spec **/
mediator gmtime_media for specification
TmTObj * gmtime_spec( CallContext context, TimeTObj * timer, ErrorCode * errno );

/** gmtime_r_spec **/
mediator gmtime_r_media for specification
TmTObj * gmtime_r_spec( CallContext context, TimeTObj * timer, TmTObj * result, ErrorCode * errno );

/** localtime_spec **/
mediator localtime_media for specification
TmTObj * localtime_spec( CallContext context, TimeTObj * timer, ErrorCode * errno );

/** localtime_r_spec **/
mediator localtime_r_media for specification
TmTObj * localtime_r_spec( CallContext context, TimeTObj * timer, TmTObj * result, ErrorCode * errno );

/** mktime_spec **/
mediator mktime_media for specification
TimeT mktime_spec( CallContext context, TmTObj * timeptr, ErrorCode * errno );

#endif
