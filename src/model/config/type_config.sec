/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 #include "config/system_config.seh"
 #include "common/common_scenario.seh"
 #include "common/common_media.seh"

/********************************************************************/
/**                          Size Of Types                         **/
/********************************************************************/

/*
 * Returns the size of the target type in bytes on the target platform.
 */
SizeT sizeof_Type( CallContext context, const char* type )
{
    SystemConfiguration* configuration;
    String* key;
    SizeTObj* size;
    SizeT res2;
    
    configuration= getSystemConfiguration(context);
    key = create_String(type);
    
    if (containsKey_Map( configuration->sizeofTypes, key ))
    {
        size = get_Map( configuration->sizeofTypes, key );
        return *size;
    }

    // Replace pseudo context
    if (context.process == 0)
    {
        context = getContext();
    }

    res2 = getSizeOfType(context,type);
    if (res2 > 0)
    {
        put_Map( configuration->sizeofTypes, key, create_SizeTObj(res2) );
    }
    return res2;
}

/*
 * Returns the size of the target type in bytes on the only target platform.
 */
SizeT sizeof_SUTType( const char* type )
{
  return sizeof_Type( create_CallContext(0,0,0), type );
}


SizeT sizeof_CharT;
SizeT sizeof_ShortT;
SizeT sizeof_IntT;
SizeT sizeof_LongT;
SizeT sizeof_LLongT;

SizeT sizeof_WCharT;

SizeT sizeof_FloatT;
SizeT sizeof_DoubleT;
SizeT sizeof_LongDoubleT;

SizeT sizeof_VoidTPtr;


CharT  min_CharT;
CharT  max_CharT;

SCharT  min_SCharT;
SCharT  max_SCharT;
UCharT  max_UCharT;

ShortT  min_ShortT;
ShortT  max_ShortT;
UShortT max_UShortT;

IntT  min_IntT;
IntT  max_IntT;
UIntT max_UIntT;

LongT  min_LongT;
LongT  max_LongT;
ULongT max_ULongT;

LLongT  min_LLongT;
LLongT  max_LLongT;
ULLongT max_ULLongT;

WCharT min_WCharT;
WCharT max_WCharT;

IntMaxT  min_IntMaxT;
IntMaxT  max_IntMaxT;
UIntMaxT max_UIntMaxT;

SSizeT max_SSizeT;
SizeT  max_SizeT;
