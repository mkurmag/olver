/*
 * Copyright (c) 2005-2006 Institute for System Programming
 * Russian Academy of Sciences
 * All rights reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef SYSTEM_CONFIGURATION_H
#define SYSTEM_CONFIGURATION_H

#include "atl/map.h"
#include "atl/long.h"
#include "config/platform_config.seh"
#include "config/model_config.seh"
#include "config/type_config.seh"
#include "config/pthread_config.seh"
#include "config/interpretation.seh"

// To be moved
/********************************************************************/
/**                       Hardware Configuration                   **/
/********************************************************************/
// endianness
typedef enum ByteOrder
{
  LittleEndian,
  BigEndian,
  PDPEndian
} ByteOrder;

ByteOrder getByteOrder(CallContext context);

#if defined(SINGLE_PLATFORM_CONFIGURATION)

extern ByteOrder sut_byte_order;

#endif /* SINGLE_PLATFORM_CONFIGURATION */


// Alignment value used by !pthread functions
#define PTHREAD_STACK_ALIGNMENT_VALUE 8


/********************************************************************/
/**                System Configuration Variable Names             **/
/********************************************************************/

/* 
 * These constants represent symbolic constants which shall be defined
 * in unistd.h header file.
 *
 *   Linux Standard Base Core Specification 3.1
 *   Copyright (c) 2004, 2005 Free Standards Group
 *
 *   See section 'Data Definitions for libc', header file 'unistd.h'.
 */


#define SUT_SC_ARG_MAX                        0
#define SUT_SC_CHILD_MAX                      1
#define SUT_SC_CLK_TCK                        2
#define SUT_SC_NGROUPS_MAX                    3
#define SUT_SC_OPEN_MAX                       4
#define SUT_SC_STREAM_MAX                     5
#define SUT_SC_TZNAME_MAX                     6
#define SUT_SC_JOB_CONTROL                    7
#define SUT_SC_SAVED_IDS                      8
#define SUT_SC_REALTIME_SIGNALS               9
#define SUT_SC_PRIORITY_SCHEDULING            10
#define SUT_SC_TIMERS                         11
#define SUT_SC_ASYNCHRONOUS_IO                12
#define SUT_SC_PRIORITIZED_IO                 13
#define SUT_SC_SYNCHRONIZED_IO                14
#define SUT_SC_FSYNC                          15
#define SUT_SC_MAPPED_FILES                   16
#define SUT_SC_MEMLOCK                        17
#define SUT_SC_MEMLOCK_RANGE                  18
#define SUT_SC_MEMORY_PROTECTION              19
#define SUT_SC_MESSAGE_PASSING                20
#define SUT_SC_SEMAPHORES                     21
#define SUT_SC_SHARED_MEMORY_OBJECTS          22
#define SUT_SC_AIO_LISTIO_MAX                 23
#define SUT_SC_AIO_MAX                        24
#define SUT_SC_AIO_PRIO_DELTA_MAX             25
#define SUT_SC_DELAYTIMER_MAX                 26
#define SUT_SC_MQ_OPEN_MAX                    27
#define SUT_SC_MQ_PRIO_MAX                    28
#define SUT_SC_VERSION                        29
#define SUT_SC_PAGESIZE                       30
#define SUT_SC_PAGE_SIZE                      30
#define SUT_SC_RTSIG_MAX                      31
#define SUT_SC_SEM_NSEMS_MAX                  32
#define SUT_SC_SEM_VALUE_MAX                  33
#define SUT_SC_SIGQUEUE_MAX                   34
#define SUT_SC_TIMER_MAX                      35
#define SUT_SC_BC_BASE_MAX                    36
#define SUT_SC_BC_DIM_MAX                     37
#define SUT_SC_BC_SCALE_MAX                   38
#define SUT_SC_BC_STRING_MAX                  39
#define SUT_SC_COLL_WEIGHTS_MAX               40
#define SUT_SC_EXPR_NEST_MAX                  42
#define SUT_SC_LINE_MAX                       43
#define SUT_SC_RE_DUP_MAX                     44
#define SUT_SC_2_VERSION                      46
#define SUT_SC_2_C_BIND                       47
#define SUT_SC_2_C_DEV                        48
#define SUT_SC_2_FORT_DEV                     49
#define SUT_SC_2_FORT_RUN                     50
#define SUT_SC_2_SW_DEV                       51
#define SUT_SC_2_LOCALEDEF                    52
#define SUT_SC_IOV_MAX                        60
#define SUT_SC_THREADS                        67
#define SUT_SC_THREAD_SAFE_FUNCTIONS          68
#define SUT_SC_GETGR_R_SIZE_MAX               69
#define SUT_SC_GETPW_R_SIZE_MAX               70
#define SUT_SC_LOGIN_NAME_MAX                 71
#define SUT_SC_TTY_NAME_MAX                   72
#define SUT_SC_THREAD_DESTRUCTOR_ITERATIONS   73
#define SUT_SC_THREAD_KEYS_MAX                74
#define SUT_SC_THREAD_STACK_MIN               75
#define SUT_SC_THREAD_THREADS_MAX             76
#define SUT_SC_THREAD_ATTR_STACKADDR          77
#define SUT_SC_THREAD_ATTR_STACKSIZE          78
#define SUT_SC_THREAD_PRIORITY_SCHEDULING     79
#define SUT_SC_THREAD_PRIO_INHERIT            80
#define SUT_SC_THREAD_PRIO_PROTECT            81
#define SUT_SC_THREAD_PROCESS_SHARED          82
#define SUT_SC_ATEXIT_MAX                     87
#define SUT_SC_PASS_MAX                       88
#define SUT_SC_XOPEN_VERSION                  89
#define SUT_SC_XOPEN_UNIX                     91
#define SUT_SC_XOPEN_CRYPT                    92
#define SUT_SC_XOPEN_ENH_I18N                 93
#define SUT_SC_XOPEN_SHM                      94
#define SUT_SC_2_CHAR_TERM                    95
#define SUT_SC_2_C_VERSION                    96
#define SUT_SC_2_UPE                          97
#define SUT_SC_XBS5_ILP32_OFF32               125
#define SUT_SC_XBS5_ILP32_OFFBIG              126
#define SUT_SC_XBS5_LP64_OFF64                127
#define SUT_SC_XBS5_LPBIG_OFFBIG              128
#define SUT_SC_XOPEN_LEGACY                   129
#define SUT_SC_XOPEN_REALTIME                 130
#define SUT_SC_XOPEN_REALTIME_THREADS         131
#define SUT_SC_ADVISORY_INFO                  132
#define SUT_SC_BARRIERS                       133
#define SUT_SC_CLOCK_SELECTION                137
#define SUT_SC_CPUTIME                        138
#define SUT_SC_THREAD_CPUTIME                 139
#define SUT_SC_MONOTONIC_CLOCK                149
#define SUT_SC_READER_WRITER_LOCKS            153
#define SUT_SC_SPIN_LOCKS                     154
#define SUT_SC_REGEXP                         155
#define SUT_SC_SHELL                          157
#define SUT_SC_SPAWN                          159
#define SUT_SC_SPORADIC_SERVER                160
#define SUT_SC_THREAD_SPORADIC_SERVER         161
#define SUT_SC_TIMEOUTS                       164
#define SUT_SC_TYPED_MEMORY_OBJECTS           165
#define SUT_SC_2_PBS_ACCOUNTING               169
#define SUT_SC_2_PBS_LOCATE                   170
#define SUT_SC_2_PBS_MESSAGE                  171
#define SUT_SC_2_PBS_TRACK                    172
#define SUT_SC_SYMLOOP_MAX                    173
#define SUT_SC_2_PBS_CHECKPOINT               175
#define SUT_SC_V6_ILP32_OFF32                 176
#define SUT_SC_V6_ILP32_OFFBIG                177
#define SUT_SC_V6_LP64_OFF64                  178
#define SUT_SC_V6_LPBIG_OFFBIG                179
#define SUT_SC_HOST_NAME_MAX                  180
#define SUT_SC_TRACE                          181
#define SUT_SC_TRACE_EVENT_FILTER             182
#define SUT_SC_TRACE_INHERIT                  183
#define SUT_SC_TRACE_LOG                      184
#define SUT_SC_RAND_MAX                       185

/********************************************************************/
/**             Path System Configuration Variable Names           **/
/********************************************************************/

/* 
 * These constants represent symbolic constants which shall be defined
 * in unistd.h header file.
 *
 *   Linux Standard Base Core Specification 3.1
 *   Copyright (c) 2004, 2005 Free Standards Group
 *
 *   See section 'Data Definitions for libc', header file 'unistd.h'.
 */

#define SUT_PC_LINK_MAX                       0
#define SUT_PC_MAX_CANON                      1
#define SUT_PC_MAX_INPUT                      2
#define SUT_PC_NAME_MAX                       3
#define SUT_PC_PATH_MAX                       4
#define SUT_PC_PIPE_BUF                       5
#define SUT_PC_CHOWN_RESTRICTED               6
#define SUT_PC_NO_TRUNC                       7
#define SUT_PC_VDISABLE                       8
#define SUT_PC_SYNC_IO                        9
#define SUT_PC_ASYNC_IO                       10
#define SUT_PC_PRIO_IO                        11
#define SUT_PC_FILESIZEBITS                   13
#define SUT_PC_REC_INCR_XFER_SIZE             14
#define SUT_PC_REC_MIN_XFER_SIZE              16
#define SUT_PC_REC_XFER_ALIGN                 17
#define SUT_PC_ALLOC_SIZE_MIN                 18
#define SUT_PC_2_SYMLINKS                     20


/********************************************************************/
/**          System Configuration Variable Special Values          **/
/********************************************************************/
#define SC_VALUE_NO_LIMIT               ((LongT)-1)
#define SC_VALUE_UNKNOWN                ((LongT)-2)


/********************************************************************/
/**                  System Configuration Variables                **/
/********************************************************************/

/*
 * Returns the current variable value on the given process.
 * If the variable has no limit SC_VALUE_NO_LIMIT is returned.
 * If the value is unknown SC_VALUE_UNKNOWN is returned.
 */
LongT getSystemConfigurationValue(CallContext context,IntT name);

/*
 * Sets the current variable value on the given process.
 */
void  setSystemConfigurationValue(CallContext context,IntT name,LongT value);


/********************************************************************/
/**             Pathname System Configuration Variables            **/
/********************************************************************/

extern Map * path_configs;

specification typedef struct PathConfig 
{
    LongT filesizebits; 
    LongT link_max;
    LongT max_canon;
    LongT max_input;
    LongT name_max;
    LongT path_max;
    LongT pipe_buf;
    LongT posix2_symlinks;
    LongT posix_alloc_size_min;
    LongT posix_rec_incr_xfer_size;
    LongT posix_rec_max_xfer_size;
    LongT posix_rec_min_xfer_size;
    LongT posix_rec_xfer_align;
    LongT symlink_max;
    LongT _posix_chown_restricted;
    LongT _posix_no_trunc;
    LongT _posix_vdisable;
    LongT _posix_async_io;
    LongT _posix_prio_io;
    LongT _posix_sync_io;
}PathConfig;

bool initPathSystemConfiguration(void);

PathConfig * getPathConfig(CallContext context, CString* path);

/*
 * Returns the current variable value on the given path and the process.
 * If the variable has no limit SC_VALUE_NO_LIMIT is returned.
 * If the value is unknown SC_VALUE_UNKNOWN is returned.
 */
LongT getPathSystemConfigurationValue(CallContext context,CString* path,IntT name);
LongT getFileDescSystemConfigurationValue(CallContext context,FileDescId file_desc_id,IntT name);

PathConfig * setPathSystemConfigurationValue(CallContext context, CString* path,IntT name, LongT value);

/********************************************************************/
/**              System Configuration Initialization               **/
/********************************************************************/
void initSystemConfiguration(void);


// Standard Hardcore Values

/********************************************************************/
/**                       System Configuration                     **/
/********************************************************************/
specification typedef struct SystemConfiguration
{
  // Standard Interpretation Configuration
  // Standard Variation 
  // Agent parameters
    // Default Parameters
  PThreadConfiguration pthreadConfiguration;
  Map*                 sizeofTypes; // String -m-> ULong
  // 'sysconf' System Configuration Variables
  Map*                 sysconf;     // IntT -m-> LongT
  // Hardware Configuration
  ByteOrder            byte_order;
  // Stack alignment
  IntT stackAlignmentValue;
  
  /*
   * Realtime signals configuration variables
   * Initialize this variables in your scenario, if you need them,
   * using __libc_current_sigrtmin_spec and __libc_current_sigrtmax_spec
   */
  /* 
   * SIGRTMIN variable contains the number of available real-time signal
   * with lowest priority.
   */
  IntT sigrtmin;

  /* 
   * SIGRTMAX variable contains the number of available real-time signal
   * with highest priority.
   */
  IntT sigrtmax;
} SystemConfiguration;


SystemConfiguration* create_SystemConfiguration(void);


/********************************************************************/
/**                  System Configuration Functions                **/
/********************************************************************/
SystemConfiguration* getSystemConfiguration( CallContext context );
SystemConfiguration* getSystemConfiguration_SystemId( SystemId systemid );
IntT getStackAlignmentValue(CallContext context);


/********************************************************************/
/**               Alignment Requirements Configuration             **/
/********************************************************************/

#if defined(SINGLE_PLATFORM_CONFIGURATION)

/*
 * Alignment requirements to the main target types.
 */

extern SizeT alignment_AnyType;


extern SizeT alignment_ShortT;
extern SizeT alignment_IntT;
extern SizeT alignment_LongT;
extern SizeT alignment_LLongT;

extern SizeT alignment_WCharT;

extern SizeT alignment_FloatT;
extern SizeT alignment_DoubleT;
extern SizeT alignment_LongDoubleT;

extern SizeT alignment_VoidTPtr;

#endif /* SINGLE_PLATFORM_CONFIGURATION */

/********************************************************************/
/**                         Special System Parent                  **/
/********************************************************************/

/*
 * Returns process id of a special system process, 
 * which inherits all children of the process terminated.
 */
ProcessId getSpecialSystemParent(SystemId system);


/************************************************************************/
/**                      Architecture Name                             **/
/************************************************************************/

const char* getArchName(int arch);

#endif

