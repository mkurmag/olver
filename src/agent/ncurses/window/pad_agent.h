/* 
 * File: ncurses/window/pad_agent.h
 * 
 * Author:
 * Version:  $Id: pad_agent.h,v 1.1 2006/04/17 09:56:48 hed Exp $
 * 
 * Copyright (c) 2005 ISP RAS.
 * 25, B.Communisticheskaya, Moscow, Russia.
 * All rights reserved.
 */

#ifndef TA_NCURSES_WINDOW_PAD_AGENT_H
#define TA_NCURSES_WINDOW_PAD_AGENT_H

#include "common/agent.h"


/********************************************************************/
/**                      Agent Initialization                      **/
/********************************************************************/
void register_ncurses_window_pad_commands(void);


#endif

