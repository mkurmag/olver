/* 
 * File: ncurses/border/border_agent.h
 * 
 * Author:
 * Version:  $Id: border_agent.h,v 1.1 2006/04/17 09:51:11 hed Exp $
 * 
 * Copyright (c) 2005 ISP RAS.
 * 25, B.Communisticheskaya, Moscow, Russia.
 * All rights reserved.
 */

#ifndef TA_NCURSES_BORDER_AGENT_H
#define TA_NCURSES_BORDER_AGENT_H

#include "common/agent.h"


/********************************************************************/
/**                      Agent Initialization                      **/
/********************************************************************/
void register_ncurses_border_commands(void);


#endif

