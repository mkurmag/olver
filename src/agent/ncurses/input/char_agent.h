/* 
 * File: ncurses/input/char_agent.h
 * 
 * Author:
 * Version:  $Id: char_agent.h,v 1.1 2006/04/17 09:55:37 hed Exp $
 * 
 * Copyright (c) 2005 ISP RAS.
 * 25, B.Communisticheskaya, Moscow, Russia.
 * All rights reserved.
 */

#ifndef TA_NCURSES_INPUT_CHAR_AGENT_H
#define TA_NCURSES_INPUT_CHAR_AGENT_H

#include "common/agent.h"


/********************************************************************/
/**                      Agent Initialization                      **/
/********************************************************************/
void register_ncurses_input_char_commands(void);


#endif

