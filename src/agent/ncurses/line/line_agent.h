/* 
 * File: ncurses/line/line_agent.h
 * 
 * Author:
 * Version:  $Id: line_agent.h,v 1.1 2006/04/17 09:55:59 hed Exp $
 * 
 * Copyright (c) 2005 ISP RAS.
 * 25, B.Communisticheskaya, Moscow, Russia.
 * All rights reserved.
 */

#ifndef TA_NCURSES_LINE_AGENT_H
#define TA_NCURSES_LINE_AGENT_H

#include "common/agent.h"


/********************************************************************/
/**                      Agent Initialization                      **/
/********************************************************************/
void register_ncurses_line_commands(void);


#endif

