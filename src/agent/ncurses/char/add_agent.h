/* 
 * File: ncurses/char/add_agent.h
 * 
 * Author:
 * Version:  $Id: add_agent.h,v 1.1 2006/04/17 09:51:23 hed Exp $
 * 
 * Copyright (c) 2005 ISP RAS.
 * 25, B.Communisticheskaya, Moscow, Russia.
 * All rights reserved.
 */

#ifndef TA_NCURSES_CHAR_ADD_AGENT_H
#define TA_NCURSES_CHAR_ADD_AGENT_H

#include "common/agent.h"


/********************************************************************/
/**                      Agent Initialization                      **/
/********************************************************************/
void register_ncurses_char_add_commands(void);


#endif

